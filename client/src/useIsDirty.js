import { useReducer } from 'react'


const handleDirty = (isDirty, newDirty = true) => !(newDirty && isDirty) ? newDirty : isDirty


export const useIsDirty = () => useReducer(handleDirty, false)
