import React, { useState, useEffect } from 'react'
import { navigate } from '@reach/router'
import { Box, Button } from 'grommet'
import dayjs from 'dayjs'

import { useThemeContext } from 'ThemeContext'
import { rest } from 'api'


const NavLink = ({ children, to, ...rest }) => <Button color="neutral-3" label={ children } onClick={() => navigate(to)} {...rest } />
const today = dayjs().format('YYYY-MM-DD')


export default function Dashboard() {
	const [dashboard, setDashboard] = useState({})
	useEffect(() => {
		rest('find', 'dashboard').then( result => setDashboard(result))
	}, [])

    const { size } = useThemeContext()

	return (
		<Box direction="row">
			<Box align="center" alignContent="center" gap="medium" pad={ size }>
				<NavLink to={`/compras/${today}/weekly`} primary>Compras</NavLink>
				<NavLink to="/insumos">Insumos</NavLink>
				<br />
				<NavLink to="/recetas" primary>Productos<br/>y Recetas</NavLink>
				<br />
				<NavLink to={`/ventas/${today}/weekly`} primary>Ventas</NavLink>
				<NavLink to={`/consumos/${today}/weekly`}>Consumos</NavLink>
			</Box>

			<Box align="center" alignContent="stretch" background="accent-4" gap="medium" pad="medium">
				<small><pre>{ JSON.stringify(dashboard, 0, 4)}</pre></small>
			</Box>
		</Box>
	)
}
