import React from 'react'
import { Grid, Box, Anchor, Text, Button } from 'grommet'
import { Home, Logout } from 'grommet-icons'

import { useLoginContext } from 'LoginContext'
import { useThemeContext } from 'ThemeContext'


const columns = {
    small: ["none", "auto", "none"],
    medium: ["xsmall", "auto", "xsmall"],
    large: ["1/4", "auto", "1/4"]
}


export default function Menu(props) {
    const { children } = props
    console.log('Menu path', props['*'])
    const { handleLogout } = useLoginContext()
    const { size } = useThemeContext()

    return (
        <Grid columns={ columns[size] }>
            <Box>&nbsp;</Box>

            <Box direction="column">
                <Box as="header" pad="medium" justify="between" direction="row" background="neutral-3">
                    <Anchor href="/"><Home /></Anchor>

                    <Text size={ size } weight="bold">Ingeniería de Menú</Text>

                    <Button onClick={ handleLogout }>
                        {/* <Text>{ login.user.login }</Text> &nbsp;  */}<Logout />
                    </Button>
                </Box>
                
                { children }

                <Box as="footer" align="center">{ size }</Box>
            </Box>

            <Box>&nbsp;</Box>
        </Grid>
    )
}
