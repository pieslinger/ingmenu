import React, { useState, useEffect } from 'react'
import { Box, DataTable, Text } from 'grommet'
import { Previous, Next } from 'grommet-icons'
import { Link } from '@reach/router'
import dayjs from 'dayjs'

import { rest } from 'api'
import { useThemeContext } from 'ThemeContext'


export default function Weekly({ navigate, thru = dayjs().format('YYYY-MM-DD') }) {
    const from = dayjs(thru).add(-6, 'day').format('YYYY-MM-DD')
    
    const [list, setList] = useState([])
    useEffect(() => {
        const days = Array.from(Array(7).keys()).map( k => dayjs(from).add(k, 'day').format('YYYY-MM-DD'))

        rest('find', 'ventas', { from, thru })
        .then( list => Array.isArray(list) ? list : [list]) //  TODO: is needed?
        .then( list => days.map( fecha => list.find( f => f.fecha===fecha ) || ({ fecha, total: 0 })))
        .then( list => setList(list))
    }, [from, thru])
    
    const prevDate = dayjs(thru).add(-1, 'week').format('YYYY-MM-DD')
    const nextDate = dayjs(thru).add(+1, 'week').format('YYYY-MM-DD')

    const renderDate  = ({ fecha, total }) => (
        <Text color={ total ? "" : "status-critical"}>
            { fecha ? dayjs(fecha).format('ddd DD') : 'Total' }
        </Text>
    )
    //  DataTable
    const { background } = useThemeContext()
    const columns = [
        { property: 'fecha',    header: 'Fecha', render: renderDate },
        { property: 'vouchers', header: 'Vouchers' },
        { property: 'total',    header: 'Total', footer: { aggregate: true }, aggregate: 'sum', align: 'end' },
    ]
    const handleClickRow = ({ datum: item }) => navigate(`../../${item.fecha}`)
    
    return (
        <Box pad="small">
            <Box as="header" pad="medium" justify="between" direction="row">
                <Link to={`../../${prevDate}/weekly`}><Previous color="neutral-1" /></Link>
                <Text color="neutral-4" weight="bold">
                    { dayjs(from).format('DD MMM.') } al { dayjs(thru).format('DD MMM.') }
                </Text>
                <Link to={`../../${nextDate}/weekly`}><Next color="neutral-1" /></Link>
            </Box>
            
            <DataTable alignSelf="stretch" background={ background } pad="small"
                columns={ columns } data={ list } 
                onClickRow={ handleClickRow } 
            />
        </Box>
	)
}
