import React, { useEffect } from 'react'

import { useVentasContext } from './Context'

import EditForm from './EditForm'


export default function Update({ navigate, id }) {
    const { item, getItem, handleUpdate } = useVentasContext()

    useEffect(() => getItem(id), [getItem, id])
    
    const handleSubmit = (value) => handleUpdate(value).then( 
        result => console.log('Submitted', result, navigate(`/ventas/${value.fecha}`))
    )
    return <EditForm title={ item.id } item={ item } onSubmit={ handleSubmit } />
}
