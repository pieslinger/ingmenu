import React from 'react'
import { Router, Link } from '@reach/router'
import { Box, Text } from 'grommet'
import dayjs from 'dayjs'

import ContextProvider from './Context'
import Weekly from './Weekly'
import Daily  from './Daily'
import Create from './Create'
import Update from './Update'


const today = dayjs().format('YYYY-MM-DD')
const Hello = () => (<h1><Link to={ today }>Hello Ventas!</Link></h1>)


export default (props) => {

    return (
        <ContextProvider service="ventas">
            <Box as="header" align="center" background="neutral-4" pad="small">
                <Text weight="bold">Ventas</Text>
            </Box>

            <Router>
                <Hello  path="/" />
                <Weekly path=":thru/weekly" />
                <Daily  path=":date" />
                <Create path=":date/create" />
                <Update path=":id/update" />

                <Daily  default />
            </Router>
        </ContextProvider>
    )
}