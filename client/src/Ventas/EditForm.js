import React, { Fragment, useRef, useState, useEffect } from 'react'
import { Grid, Box, Heading, FormField, Text, Select, TextInput, Button } from 'grommet'

import { useIsDirty } from 'useIsDirty'
import { useProductosContext } from 'Productos/Context'


export default function EditForm({ item, onSubmit }) {
    const [isDirty, setIsDirty] = useIsDirty()

    const [value, setValue] = useState({ id: 0, list: [] })
    useEffect(() => setValue({...item, list: null }), [item]) //  TODO: decide what to do with value and value.list
    
    const refs = useRef({}) //  for item.list

    const { options } = useProductosContext()

    const [option, setOption] = useState({value: 0, label: '' })
    useEffect(() => {
        item.id_categoria && options.length &&
        setOption({...options.find( f => f.value===item.id_categoria ) }) //  initialValue
    }, [item, options])

    if (item.id!==value.id) return null

    const handleChange = ({ target, option }) => {
        setIsDirty()
        if (target.placeholder==='cantidad') {
            //  it's one of the uncontrolled inputs!
            target.value = Number(target.value)
        } else {
            //  controlled - TODO: do not mix!
            setValue({...value, [target.name]: option.value || target.value })
            if (option) setOption(option)
        }
    }
    const handleReset  = () => { setIsDirty(false); setValue(item) }
    const handleSubmit = () => { setIsDirty(false); 
        const items = item.list.filter( f => 
            refs.current[f.id] && 
            refs.current[f.id].value && 
            refs.current[f.id].value > "0"
        ).map( m => ({ id_producto: m.id, cantidad: Number(refs.current[m.id].value) }))

        onSubmit({...value, list: items }) 
    }
    const list = item.list.filter( f => f.pid_categoria===value.id_categoria )
    const tipos = Array.from(new Set(list.map( m => m.tipo )))

    return (
        <Box pad="medium" animation="fadeIn">
            <Grid columns={["1/3", "2/3"]} rows="xxsmall">
                <FormField label="Fecha" />
                <TextInput type="date" name="fecha" value={ value.fecha || '' } onChange={ handleChange } required />

                <FormField label="Voucher" />
                { value.id ?
                    <TextInput value={ option.label || '' } readOnly />
                :
                    <Select name="id_categoria" value={ option }
                        options={ options.filter( f => f.disabled ) || [] } valueKey="value" labelKey="label"
                        onChange={ handleChange } required 
                    />
                }
            </Grid>

            <Grid columns={["3/4", "1/4"]} rows="xxsmall" key={ value.id } margin={{ vertical: 'small' }}>
            { tipos.map( tipo => (
                <Fragment key={ tipo }>
                    <Heading level="4">{ tipo }</Heading>
                    <Heading title="stub"></Heading>

                    { list.filter( f => f.tipo===tipo ).map( m => (
                        <Fragment key={m.id}>
                            {/* <FormField label={ m && m.producto ? m.producto.substr(0, 30) +'...' : '' } /> */}
                            <FormField><Text truncate={true}>{ m.producto }</Text></FormField>
                                
                            <TextInput id={ m.id.toString() } placeholder="cantidad" defaultValue={ m.cantidad } 
                                ref={ element => refs.current[m.id] = element } onChange={ handleChange }
                            />
                        </Fragment>
                    ))}
                </Fragment>
            ))}
            </Grid>
            
            <Box justify="between" direction="row">
                <Button label="Reset"    onClick={ handleReset } />
                <Button label="Confirma" onClick={ handleSubmit } disabled={! isDirty } primary />
            </Box>
        </Box>
	)
}
