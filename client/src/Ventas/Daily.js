import React, { useState, useEffect } from 'react'
import dayjs from 'dayjs'
import { Box, DataTable, Text } from 'grommet'
import { Up, Previous, Next, AddCircle } from 'grommet-icons'
import { Link } from '@reach/router'

import { rest } from 'api'
import { useThemeContext } from 'ThemeContext'


export default function Daily({ navigate, date = dayjs().format('YYYY-MM-DD') }) {
    const [list, setList] = useState([])
    useEffect(() => {
        date.match(/20\d\d-[0-1]\d-[0-3]\d/) &&
        rest('find', 'ventas', { date })
        .then( list => Array.isArray(list) ? list : [list])
        .then( list => setList(list))
    }, [date])

    const prevDate = dayjs(date).add(-1, 'day').format('YYYY-MM-DD')
    const nextDate = dayjs(date).add(+1, 'day').format('YYYY-MM-DD')

    //  DataTable
    const { background } = useThemeContext()
    const columns = [
        { property: 'categoria', header: 'Categoria', footer: 'Total' },
        { property: 'tipo',      header: 'Tipo' },
        { property: 'cantidad',  header: 'Cantidad', footer: { aggregate: true }, aggregate: 'sum', align: 'end' },
    ]
    const handleClickRow = ({ datum: item }) => navigate(`../${item.id}/update`)
    
    return (
        <Box pad="small" background="light-1">
            <Box as="header" pad="medium" justify="between" direction="row">
                <Link to={`../${date}/weekly`}><Up /></Link>
                <Link to={`../${prevDate}`}><Previous /></Link>

                <Text weight="bold">{ date }</Text>
                
                <Link to={`../${nextDate}`}><Next /></Link>
                <Link to={`../${date}/create`} state={{ date: date }}><AddCircle /></Link>
            </Box>

            <Box>
                <DataTable alignSelf="stretch" background={ background } size="medium" 
                    columns={ columns } data={ list } 
                    onClickRow={ handleClickRow } 
                />
            </Box>
        </Box>
	)
}
