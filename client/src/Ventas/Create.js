import React, { useEffect } from 'react'

import { useVentasContext } from './Context'

import EditForm from './EditForm'


export default function Create({ navigate, date }) {
    const { item, getItem, handleCreate } = useVentasContext()

    useEffect(() => getItem(0), [getItem])
    
    const handleSubmit = (value) => handleCreate({...value, id: null }).then( 
        result => console.log('Submitted', result, navigate(`/ventas/${value.fecha}`))
    )
    return <EditForm title="Nueva Venta" item={{...item, fecha: date }} onSubmit={ handleSubmit } />
}
