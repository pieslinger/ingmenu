import React from 'react'
import { render } from 'react-dom'
import { Grommet } from 'grommet'
import { grommet } from 'grommet/themes'
import { deepMerge } from 'grommet/utils'

import ThemeContextProvider from 'ThemeContext'
import LoginContextProvider from 'LoginContext'
import * as serviceWorker from './serviceWorker'

import App from './App'


// set custom breakpoints so we can see the changes
const customBreakpoints = deepMerge(grommet, { global: { breakpoints: {
    small:  { value:  720 },
    medium: { value: 1200 },
    large: 3000
}}})

console.log('Dark mode:', window.matchMedia && window.matchMedia('(prefers-color-scheme: dark)').matches)
console.log ('Environment:', process.env)


render(
    <Grommet full theme={ customBreakpoints }>
        <ThemeContextProvider>
            <LoginContextProvider children={ <App /> } />
        </ThemeContextProvider>
    </Grommet>
, document.getElementById('root'))

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister()
