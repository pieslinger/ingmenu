//  the REST API setup
import axios from 'axios'

//  the Socket API setup
// import io from "socket.io-client"


const { protocol, hostname } = window.location
const baseURL = `${protocol}//${hostname}:${process.env.REACT_APP_SERVER_PORT}`
const api = axios.create({ baseURL })

api.interceptors.response.use( response => {
    // simply return the response if there is no error
    return response;
}, error => {
    if (error.response.status === 401) {
    }
    return Promise.reject(error);
})


export const rest = async (method, service, params) => {

    if (method==='find') {
        method = 'get'
        params = { params }
    }
    if (method==='create') {
        method = 'post'
    }
    if (method==='update') {
        method = 'put'
    }
    const data = await api[method](`/${service}`, params)
    .then( done => done.data )
    .catch( err => ({ error: JSON.stringify(err), message: `Some error in REST call to ${method}/${service}` }))
    
    return data
}
// export const socket = io(process.env.REACT_APP_BASE_URL)
// export const emit = (method, service, params) => {
    
//     return new Promise((resolve, reject) => {
//         const resolveReject = (error, data) => error ? reject(error) : resolve(data)

//         socket.emit(method, service, params, resolveReject)
//     })
// }
