import React from 'react'
import { Router } from '@reach/router'

import ContextProvider from './Context'

import Browse from './Browse'
import Update from './Update'


const Compras = ({ id }) => <h1>Compras del insumo { id }</h1>
const Recetas = ({ id }) => <h1>Recetas del insumo { id }</h1>


export default (props) => {

    return (
        <ContextProvider>
            <Router>
                <Browse path="/" />
                <Update path=":id" />

                <Compras path=":id/compras" />
                <Recetas path=":id/recetas" />
            </Router>
        </ContextProvider>
    )
}