import React, { useReducer, useState, useEffect } from 'react'
import { Grid, Box, FormField, TextInput, Button } from 'grommet'
import { navigate } from '@reach/router'


export default function EditForm({ item, onSubmit, onClick }) {
    const [isDirty, setIsDirty] = useReducer(
        (isDirty, newDirty = true) => !(newDirty && isDirty) ? newDirty : isDirty
    , false)
    const [value, setValue] = useState(item)
    useEffect(() => setValue(item), [item])

    if (! value) return null
    
    const handleChange = ({ target, option }) => {
        setIsDirty()
        setValue({...value, [target.name]: option || target.value })
    }
    const handleReset  = () => { setIsDirty(false); setValue(item)  }
    const handleSubmit = () => { setIsDirty(false); onSubmit(value) }
    const handleClick  = ({ target }) => {
        if (target.id=='compras') {
            navigate(`/compras/${item.id}/filter`)
        }
        if (target.id=='recetas') {
            console.log('clicked on', target.id, onClick(target.id))
        }
    } 
    
    // const handleClick  = (a) => (b) => console.log('a', a, 'b', b)
    return (
        <Box pad="medium" animation="fadeIn">
            <TextInput type="text" name="insumo" value={ value.insumo || '' } onChange={ handleChange } plain readOnly />

            <Grid columns={["1/4", "1/4", "1/4", "1/4"]} rows="xsmall" margin={{ horizontal: 'small', vertical: 'small'}}>
                <FormField label="Uni. Compra" />
                <TextInput name="compra" value={ value.compra || '' } onChange={ handleChange } required />

                <FormField label="Uni. Receta" />
                <TextInput name="receta" value={ value.receta || '' } onChange={ handleChange } required />

                <FormField label="Factor" />
                <TextInput name="factor" value={ value.factor || 1  } onChange={ handleChange } min="1" required />

                <FormField label="Parent ID" />
                <TextInput name="pid" value={ value.pid || undefined} onChange={ handleChange } />
            </Grid>
            
            <Box justify="between" direction="row">
                <Button label="Reset"    onClick={ handleReset  } />
                <Button label="Confirma" onClick={ handleSubmit } disabled={! isDirty } primary />
            </Box>
            
            <Grid columns={["1/4", "1/4", "1/4", "1/4"]} rows="xxsmall" margin={{ horizontal: 'small', top: 'medium' }}>
                <FormField label="Compras" />
                <Button id="compras" label={ value.compras } onClick={ handleClick } color="dark-4" />

                <FormField label="Recetas" />
                <Button id="recetas" label={ value.recetas } onClick={ handleClick } color="dark-4" />
            </Grid>
        </Box>
	)
}
