import React from 'react'
import { Grid, Box, Text, CheckBox, Button } from 'grommet'
import { navigate } from '@reach/router'

import { useInsumosContext } from './Context'
import { useThemeContext } from 'ThemeContext'

import Spinner from 'Spinner'


export default function Browse() {
    const { list } = useInsumosContext()
    const [checked, setChecked] = React.useState(false)
    
    //  DataTable
    const { background } = useThemeContext()
    const handleClickRow = ({ datum: item }) => navigate(`/insumos/${item.id}`)

    return (
        <Box>
            <Box as="header" background="neutral-4" direction="row" justify="between" pad="small">
                <Text weight="bold">Insumos (en Recetas)</Text>

                <CheckBox checked={ checked } label="Revisar" onChange={({ target }) => setChecked(target.checked)} />
            </Box>
            { list.length===0 ? <Spinner />
            : list.filter( f => ! checked || ! f.compra || ! f.receta ).map((m, i) => (
                <Grid key={i} columns={["1/2", "1/4", "1/4"]} onClick={() => handleClickRow({ datum: m })}>
                    <Box background={ background.body[i%2] } pad="small"><Button plain hoverIndicator={true} label={ m.insumo } /></Box>
                    <Box background={ background.body[i%2] } pad="small"><Button plain hoverIndicator={true} label={`1 ${m.compra}`} /></Box>
                    <Box background={ background.body[i%2] } pad="small"><Button plain hoverIndicator={true} label={`${m.factor} ${m.receta}`} /></Box>
                </Grid>
            ))}
        </Box>
    )
}
