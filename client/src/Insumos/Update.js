import React, { useEffect } from 'react'

import { useInsumosContext } from './Context'

import EditForm from './EditForm'


export default function Update({ navigate, id }) {
    const { item, getItem, handleUpdate } = useInsumosContext()

    useEffect(() => getItem(id), [getItem, id])
    
    const handleSubmit = (value) => handleUpdate(value).then( 
        result => console.log('Submitted', result, navigate(`../`))
    )
    const handleClick = (path) => navigate(path)
    
    return <EditForm title={ item.id } item={ item } onSubmit={ handleSubmit } onClick={ handleClick } />
}
