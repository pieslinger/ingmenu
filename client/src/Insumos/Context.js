import React, { createContext, useContext, useState, useEffect } from 'react'

import { useLoginContext } from 'LoginContext'
import { rest } from 'api'

const Context = createContext()


export const useInsumosContext = () => useContext(Context)
export default function ContextProvider({ children }) {
    const { login } = useLoginContext()

    const [id,   getItem] = useState(0)
    const [item, setItem] = useState({})
    const [list, setList] = useState([])
    useEffect(() => {
        rest('find', 'insumos').then( data => setList(data))
    }, [])
    useEffect(() => {
        id && 
        rest('get', `insumos/${id}`).then( data => setItem(data))
    }, [id])

    const handles = {
        handleUpdate: (item) => {
            item = {...item, id_admin: login.id }   //  TODO: add id_admin in table
            
            return rest('update', `insumos/${item.id}`, item)
            .then( data => { 
                getItem(0)
                setList(list.map( m => m.id!==item.id ? m : item ))
                return data 
            })
            .catch( err => ({...item, error: err }))
        },
    }
	return <Context.Provider value={{ item, getItem, list, ...handles }} children={ children } />
}
