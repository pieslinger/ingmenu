import React from 'react'
import { Box, Form, FormField, Button } from 'grommet'

import { useLoginContext } from './LoginContext'


export default function Login({ navigate }) {
    const { handleLogin } = useLoginContext()

    const handleSubmit = event => {
        console.log('handleSubmit', event)
        handleLogin(event)
        .then( result => {
            if (result) navigate('/')
        })
    } 

	return (
        <Box align="center" pad="medium" background="light-1">
            <Form onSubmit={ handleSubmit }>
                <FormField label="Nombre" name="login" type="text"     required />
                <FormField label="Clave"  name="clave" type="password" required />

                <Box justify="between" direction="row">
                    <Button type="reset"  label="Reset" />
                    <Button type="submit" label="Ingresar" primary />
                </Box>
            </Form>
        </Box>
	)
}
