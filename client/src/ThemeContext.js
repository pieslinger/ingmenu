import React, { createContext, useContext } from 'react'
import { ResponsiveContext } from 'grommet'


const Context = createContext()


export const useThemeContext = () => useContext(Context)
export default function ContextProvider({ children }) {
    const background = {
        header: "dark-3",
        body: ["light-1", "light-3"],
        footer: "dark-3"
    }
    const size = useContext(ResponsiveContext)

    return <Context.Provider value={{ background, size }} children={ children } />
}
