import React, { useState, useEffect } from 'react'
import { Select, Box, Text } from 'grommet'
import { FormAdd } from 'grommet-icons'


export default function Typeahead({ initialValue, onSearch, onChange }) {
    const [valueKey, labelKey] = Object.keys(initialValue)

    const [options, setOptions] = useState([])
    const [value, setValue] = useState({ [valueKey]: '', [labelKey]: '' })
    useEffect(() => {
        if (initialValue && initialValue[valueKey]) {
            setOptions([initialValue])
            setValue(initialValue)
        }
    }, [initialValue, valueKey])
    
    const handleOption = (option) => {

        return (
            <Box direction="row" justify="between" pad="medium" gap="medium">
                <Text color={ option[valueKey] < 0 ? "status-critical" : "" }>
                    { option[labelKey] }
                </Text> 
                &nbsp;{ option[valueKey] < 0 ? <FormAdd /> : null }
            </Box>
        )
    }
    const handleSearch = (typeahead) => {
        if (! typeahead) return setOptions([])

        onSearch(typeahead)
        .then( list => setOptions(list))
    }
    const handleChange = ({ value }) => { setValue(value); onChange(value) }

    return (
        <Select options={ options } value={ value } valueKey={ valueKey } labelKey={ labelKey }
            children={ handleOption }
            onSearch={ handleSearch } searchPlaceholder="Buscar..."
            onChange={ handleChange }
        />
    )
}
