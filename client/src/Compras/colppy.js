import { useState, useEffect } from 'react'
import axios from 'axios'


const api = axios.create({
    baseURL: 'https://login.colppy.com/lib/frontera2' 
})

const Usuario = {
    iniciar_sesion: {
        "service": {
            "provision": "Usuario",
            "operacion": "iniciar_sesion"
        },
        "auth": {
            "usuario": "klaus@pieslinger.net",
            "password": "fb2f020d9356e813822757e15eb2f6d9"
        },
        "parameters": {
            "usuario": "klaus@pieslinger.net",
            "password": "39bb37cf36d3b29a9280d8a70a0eed42"
        }
    }
}
const iniciarSesion = () => {
    return api.post('/service.php', Usuario.iniciar_sesion)
    .then( done => done.data )
    .then( data => data.response || { success: false })
    .then( response => response.success && response.data )
    .catch( err => console.log('Colppy Usuario.iniciar_sesion', err))
}

const Proveedor = {
    listar_proveedor: {
        "auth": {
            "usuario": "klaus@pieslinger.net",
            "password": "fb2f020d9356e813822757e15eb2f6d9"
        },
        "service": {
            "provision": "Proveedor",
            "operacion": "listar_proveedor"
        },
        "parameters": {
            "sesion": {
                "usuario": "klaus@pieslinger.net",
                "claveSesion": "-- tbc --"
            },
            "idEmpresa": "5776",
            "start": 0,
            "limit": 50,
            "order":[{
                "field":"NombreFantasia",
                "order":"ASC"
            }],
            "filter": [
                {"field":"RazonSocial", "op":"%%", "value":"JORGE"}, 
                {"field":"NombreFantasia", "op":"%%", "value":"JORGE"}
            ]
        }
    }
}  
const listarProveedor = (sesion, fuzz) => {
    if (! sesion) return [{ error: 'no session' }]
    if (! fuzz) return [{ error: 'no fuzz' }]

    const { parameters } = Proveedor.listar_proveedor

    parameters.sesion.claveSesion = sesion
    parameters.filter[0].value = fuzz
    parameters.filter[1].value = fuzz

    return api.post('/service.php', {...Proveedor.listar_proveedor, parameters })
    .catch( err => console.log('Colppy Proveedor.listar_proveedor', err))
    .then( done => done.data )
    .then( data => data.response || { success: false })
    .then( response => response.success && response.data )
    .then( data => data.map( m => ({ idProveedor: m.idProveedor, 
            RazonSocial: m.RazonSocial +(m.RazonSocial===m.NombreFantasia ? '' : ` (${m.NombreFantasia})`) 
        })
    ))
}

export default function useColppy() {
    const [sesion, setSesion] = useState(sessionStorage.sesion || '')
    const getSessionId = () => iniciarSesion().then( data => setSesion(data ? data.claveSesion : ''))
    
    useEffect(() => {
        getSessionId()
    }, [])
    useEffect(() => {
        sessionStorage.sesion = sesion
    }, [sesion])

    const onSearch = (fuzz) => listarProveedor(sesion, fuzz)

    return { sesion, onSearch }
}
