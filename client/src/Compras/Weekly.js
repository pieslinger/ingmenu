import React, { Fragment, useState, useEffect } from 'react'
import { Box, DataTable, Text } from 'grommet'
import { Up, Previous, Next, Search } from 'grommet-icons'
import { Link } from '@reach/router'
import dayjs from 'dayjs'

import { rest } from 'api'
import { useThemeContext } from 'ThemeContext'


export default function Weekly({ navigate, thru = dayjs().format('YYYY-MM-DD') }) {
    const from = dayjs(thru).add(-6, 'day').format('YYYY-MM-DD')
    
    const [list, setList] = useState([])
    useEffect(() => {
        const days = Array.from(Array(7).keys()).map( k => dayjs(from).add(k, 'day').format('YYYY-MM-DD'))

        rest('find', 'compras', { from, thru })
        .then( list => Array.isArray(list) ? list : [list])
        .then( list => days.map( fecha => list.find( f => f.fecha===fecha ) || ({ fecha, total: 0 })))
        .then( list => setList(list))
    }, [from, thru])
    
    const prevDate = dayjs(thru).add(-1, 'week').format('YYYY-MM-DD')
    const nextDate = dayjs(thru).add(+1, 'week').format('YYYY-MM-DD')

    const renderDate  = ({ fecha, total }) => (
        <Text color={ total ? "" : "status-critical"}>
            { fecha ? dayjs(fecha).format('ddd DD') : '' }
        </Text>
    )
    //  DataTable
    const { background } = useThemeContext()
    
    const columns = [
        { property: 'fecha', header: 'Fecha', render: renderDate },
        { property: 'total', header: 'Total', render: (item) => item.total.toFixed(2), 
            footer: { aggregate: true }, aggregate: 'sum', align: 'end' 
        },
    ]
    const handleClickRow = ({ datum: item }) => navigate(`../../${item.fecha}`)
    
    return (
        <Fragment>
            <Box as="header" pad="medium" justify="between" direction="row">
                <Link to={`/`}><Up /></Link>
                <Link to={`../../${prevDate}/weekly`}><Previous color="neutral-1" /></Link>

                <Text weight="bold">{ dayjs(from).format('DD MMM.') } al { dayjs(thru).format('DD MMM.') }</Text>

                <Link to={`../../${nextDate}/weekly`}><Next color="neutral-1" /></Link>
                <Link to={`../../search`}><Search /></Link>
            </Box>
            
            <Box>
                <DataTable alignSelf="stretch" background={ background } pad="small"
                    columns={ columns } data={ list } 
                    onClickRow={ handleClickRow } 
                />
            </Box>
        </Fragment>
	)
}
