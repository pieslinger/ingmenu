import React from 'react'

import Typeahead from 'Typeahead'

import useColppy from './colppy'


export default function Proveedor(props) {
    const { onSearch } = useColppy()

    const handleSearch = (typeahead) => onSearch(typeahead)
    
    return <Typeahead {...props } onSearch={ handleSearch } />
}
