import React, { Fragment, useState, useEffect } from 'react'
import { Grid, Box, FormField, DataTable } from 'grommet'
import dayjs from 'dayjs'

import { rest } from 'api'
import Typeahead from './Proveedor'
import { useThemeContext } from 'ThemeContext'


export default function Search({ navigate, thru = dayjs().format('YYYY-MM-DD') }) {
    // const from = dayjs(thru).add(-6, 'day').format('YYYY-MM-DD')
    
    const [value, setValue] = useState({})
    const handleTypeahead = ({ idProveedor, RazonSocial }) => {
        setValue({...value, id_proveedor: Number(idProveedor), RazonSocial })

        return Number(idProveedor)
    }

    const [list, setList] = useState([])
    useEffect(() => {
        value.id_proveedor &&
        rest('find', 'compras', { id_proveedor: value.id_proveedor })
        .then( list => Array.isArray(list) ? list : [list])
        .then( list => setList(list))
    }, [value])
    
    //  DataTable
    const { background } = useThemeContext()
    const columns = [
        { property: 'id',    header: 'Fecha', render: ({ fecha }) => (dayjs(fecha).format('ddd DD')) },
        { property: 'documento', header: 'Documento' },
        { property: 'total', header: 'Total', footer: { aggregate: true }, aggregate: 'sum', align: 'end' },
    ]
    const handleClickRow = ({ datum: item }) => navigate(`../${item.id}/update`)
    
    return (
        <Fragment>
            <Box as="header" pad="medium">
                <Grid columns={["1/3", "2/3"]} rows="xxsmall">
                    <FormField label="Proveedor" />
                    <Typeahead initialValue={{ idProveedor: 0, RazonSocial: 'Buscar...' }}
                        onChange={ handleTypeahead } required
                    />
                </Grid>
            </Box>
            <br />
            <Box>
            { ! list.length ? null
            :   <DataTable alignSelf="stretch" background={ background } pad="small"
                    columns={ columns } data={ list } 
                    onClickRow={ handleClickRow } 
                />
            }
            </Box>
        </Fragment>
	)
}
