import React from 'react'

import Typeahead from 'Typeahead'

import { rest } from 'api'


export default function Insumo(props) {
    const [valueKey, labelKey] = Object.keys(props.initialValue)

    const handleSearch = (typeahead) => rest('find', 'insumos', { 
        field: labelKey, fuzzy: typeahead 
    }).then( 
        list => list.length ? list : [{ [valueKey]: -1, [labelKey]: typeahead }]
    )
    return <Typeahead {...props } onSearch={ handleSearch } />
}
