import React, { useState, useReducer } from 'react'
import { Box, Grid, FormField, TextInput, Button } from 'grommet'

import Typeahead from './Insumo'


const handleDirty = (isDirty, newDirty = true) => !(newDirty && isDirty) ? newDirty : isDirty


export default function EditForm({ item, onSubmit }) {
    const [value, setValue] = useState(item)
    const [isDirty, setIsDirty] = useReducer(handleDirty, false)

    const handleChange = ({ target }) => {
        if (target && target.name) {
            setIsDirty()
            
            setValue({...value, [target.name]: target.value })
        }
        return (target && target.name)
    }
    const handleReset  = () => { setIsDirty(false); onSubmit(null)  }
    const handleSubmit = () => { setIsDirty(false); onSubmit(value) }

    const handleTypeahead = (props) => {
        if (! props) return null

        const { id, insumo, compra } = props

        setValue({...value, id_insumo: Number(id), insumo, unidades: compra })
    } 
    return (
        <Box animation="fadeIn" background="light-4" pad="medium" round="true">
            <Grid columns={{ count: 2, size: 'auto', gap: 'small' }}>
                <FormField label="Insumo" />
                <Typeahead initialValue={{ id: value.id_insumo, insumo: value.insumo }}
                    onChange={ handleTypeahead } required 
                />
            </Grid>
            <Grid columns={["1/2", "1/4", "1/4"]} pad="small">
                <FormField label="Cantidad" />
                <TextInput type="number" name="cantidad" value={ value.cantidad } onChange={ handleChange } min="0" required />
                <FormField label={ value.unidades } />
            </Grid>
            <Grid columns={{ count: 2, size: 'auto', gap: 'small' }}>
                <FormField label="Costo" />
                <TextInput type="number" name="costo" value={ value.costo } onChange={ handleChange } min="0" required />
            </Grid>
            <br />
            <Box justify="between" direction="row">
                <Button label="Reset"    onClick={ handleReset } />
                <Button label="Confirma" onClick={ handleSubmit } disabled={! isDirty } primary />
            </Box>
        </Box>
	)
}
