import React, { useState } from 'react'
import { Box, DataTable, Button } from 'grommet'
import { AddCircle } from 'grommet-icons'

import { useThemeContext } from 'ThemeContext'
import { useComprasContext } from 'Compras/Context'

import EditForm from './EditForm'


export default function Insumos({ list, onChange }) {
    //  DataTable
    const { background } = useThemeContext()
    const columns = [
        { property: 'insumo',   header: 'Insumo', footer: 'Total' },
        { property: 'cantidad', header: 'Cantidad', render: (item) => item.cantidad && item.cantidad.toFixed(3),
            align: 'end' 
        },
        { property: 'unidades', header: 'Unidades' },
        { property: 'costo',    header: 'Costo', render: (item) => item.costo.toFixed(2),
            footer: { aggregate: true }, aggregate: 'sum', align: 'end' 
        },
    ]
    const { item:{ id: id_compra }} = useComprasContext()

    const [item, setItem] = useState(null)  //  Detail
    //  Form
    const handleSubmit = (value) => {
        setItem(null)
        
        if (value) {
            const target = { name: 'list' }
            const cleanup = ['cantidad', 'costo'].reduce((r, k) => ({...r, [k]: Number(value[k]) }), {})

            Object.assign(value, cleanup)

            target.value = list.filter( f => f.id_insumo!==value.id_insumo || f.insumo.toLowerCase()!==value.insumo.toLowerCase() )
            if (value.cantidad * value.costo > 0) {
                target.value.push(value)
            }
            onChange({ target })
        }
        return value
    } 
    //  List
    const handleClickAdd = () => setItem({ id_compra, id_insumo: undefined, cantidad: '', unidades: '', costo: undefined })
    const handleClickRow = ({ datum }) => setItem(datum)

    if (item)
        return <EditForm item={ item } onSubmit={ handleSubmit } />
    else
        return (
            <Box background="light-1" direction="column">
                <DataTable alignSelf="stretch" background={ background } size="medium" 
                    columns={ columns } data={ list } 
                    onClickRow={ handleClickRow } 
                />
                <Button icon={<AddCircle />} plain onClick={ handleClickAdd } />
            </Box>
        )
}
