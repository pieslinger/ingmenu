import React, { Fragment, useState, useEffect } from 'react'
import { Grid, Box, Heading, FormField, TextInput, Button } from 'grommet'

import { useIsDirty } from 'useIsDirty'

import Typeahead from './Proveedor'
import Insumos from './Insumos'


export default function EditForm({ item, onSubmit }) {
    const [isDirty, setIsDirty] = useIsDirty()
    const [value, setValue] = useState(item)
    useEffect(() => setValue(item), [item])

    const handleChange = ({ target }) => {
        if (target && target.name) {
            setIsDirty()
            setValue({...value, [target.name]: target.value })
        }
        return (target && target.name)
    }
    const handleReset  = () => { setIsDirty(false); setValue(item);  return item }
    const handleSubmit = () => { setIsDirty(false); onSubmit(value); return value }
    const handleTypeahead = ({ idProveedor, RazonSocial }) => {
        setIsDirty()
        setValue({...value, id_proveedor: Number(idProveedor), RazonSocial })

        return Number(idProveedor)
    }
    
    return (
        <Fragment>
            <Heading color="brand" level="2">Modificar Compra</Heading>

            <Grid columns={["1/3", "2/3"]} rows="xxsmall">
                <FormField label="Fecha" />
                <TextInput type="date" name="fecha" value={ value.fecha || '' } onChange={ handleChange } required />

                <FormField label="Proveedor" />
                <Typeahead initialValue={{ idProveedor: value.id_proveedor, RazonSocial: value.RazonSocial }}
                    onChange={ handleTypeahead } required
                />
                <FormField label="Documento" />
                <TextInput type="text" name="documento" value={ value.documento || '' } onChange={ handleChange } required />
            </Grid>
            <br />
            <Insumos list={ value.list } onChange={ handleChange } />
            <br />
            <Box justify="between" direction="row">
                <Button label="Reset"    onClick={ handleReset } />
                <Button label="Confirma" onClick={ handleSubmit } disabled={! isDirty } primary />
            </Box>
        </Fragment>
	)
}
