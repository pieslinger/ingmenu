import React, { useEffect } from 'react'

import { useComprasContext } from './Context'

import EditForm from './EditForm'


export default function Create({ navigate, date }) {
    const { item, getItem, handleCreate } = useComprasContext()

    useEffect(() => getItem(0), [getItem])
    
    const handleSubmit = (value) => handleCreate({...value, id: null }).then( 
        result => console.log('Submitted', result, navigate(`/compras/${value.fecha}`))
    )
    return <EditForm title="Nueva Compra" item={{...item, fecha: date }} onSubmit={ handleSubmit } />
}
