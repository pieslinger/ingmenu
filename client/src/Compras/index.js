import React from 'react'
import { Box } from 'grommet'
import { Router } from '@reach/router'

import ContextProvider from './Context'

import Search from './Search'
import Weekly from './Weekly'
import Daily  from './Daily'
import Filter from './Filter'
import Create from './Create'
import Update from './Update'


export default (props) => {

    return (
        <ContextProvider service="compras">
            <Router>
                <Box path="/" animation="fadeIn" background="light-1" pad="medium">
                    <Search path="search" />
                    <Weekly path=":thru/weekly" />
                    <Daily  path=":date" />
                    <Filter path=":id_insumo/filter" />
                    <Create path=":date/create" />
                    <Update path=":id/update" />
                </Box>
            </Router>
        </ContextProvider>
    )
}