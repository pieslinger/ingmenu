import React, { Fragment, useState, useEffect } from 'react'
import { Box, DataTable, Text } from 'grommet'

import { rest } from 'api'
import { useThemeContext } from 'ThemeContext'


export default function Filter({ navigate, id_insumo }) {
    const [list, setList] = useState([])
    useEffect(() => {
        rest('find', 'compras', { id_insumo })
        .then( list => Array.isArray(list) ? list : [list]) //  TODO: ?
        .then( list => setList(list))
    }, [id_insumo])

    //  DataTable
    const { background } = useThemeContext()

    const columns = [
        { property: 'id', header: 'Proveedor', footer: "Total", render: (datum) => datum.RazonSocial },
        { property: 'documento',   header: 'Documento' },
        { property: 'total', header: 'Total', render: (item) => item.total.toFixed(2),
            footer: { aggregate: true }, aggregate: 'sum', align: 'end' 
        },
    ]
    const handleClickRow = ({ datum: item }) => navigate(`../${item.id}/update`)
    
    return (
        <Fragment>
            <Box as="header" pad="medium" justify="between" direction="row">
                <Text weight="bold">{ id_insumo }</Text>
            </Box>

            <Box>
                <DataTable alignSelf="stretch" background={ background } size="medium" 
                    columns={ columns } data={ list } 
                    onClickRow={ handleClickRow } 
                />
            </Box>
        </Fragment>
	)
}
