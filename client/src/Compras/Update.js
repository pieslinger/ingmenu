import React, { useEffect } from 'react'

import { useComprasContext } from './Context'

import EditForm from './EditForm'


export default function Update({ navigate, id }) {
    const { item, getItem, handleUpdate } = useComprasContext()

    useEffect(() => getItem(id), [getItem, id])
    
    const handleSubmit = (value) => handleUpdate(value).then( 
        result => console.log('Submitted', result, navigate(`/compras/${value.fecha}`))
    )
    return <EditForm title={ item.id } item={ item } onSubmit={ handleSubmit } />
}
