import React, { useState, useEffect } from 'react'
import axios from 'axios'


const api = axios.create({
    baseURL: 'https://login.colppy.com/lib/frontera2' 
})
const Usuario = {
    iniciar_sesion: {
        "service": {
            "provision": "Usuario",
            "operacion": "iniciar_sesion"
        },
        "auth": {
            "usuario": "klaus@pieslinger.net",
            "password": "fb2f020d9356e813822757e15eb2f6d9"
        },
        "parameters": {
            "usuario": "klaus@pieslinger.net",
            "password": "39bb37cf36d3b29a9280d8a70a0eed42"
        }
    }
}
const Proveedor = {
    listar_proveedor: {
        "auth": {
            "usuario": "klaus@pieslinger.net",
            "password": "fb2f020d9356e813822757e15eb2f6d9"
        },
        "service": {
            "provision": "Proveedor",
            "operacion": "listar_proveedor"
        },
        "parameters": {
            "sesion": {
                "usuario": "klaus@pieslinger.net",
                "claveSesion": "-- tbc --"
            },
            "idEmpresa": "5776",
            "start": 0,
            "limit": 50,
            "order":[{
                "field":"NombreFantasia",
                "order":"ASC"
            }],
            "filter": [
                {"field":"RazonSocial", "op":"%%", "value":"JORGE"}, 
                {"field":"NombreFantasia", "op":"%%", "value":"JORGE"}
            ]
        }
    }
}  

export default () => {
    const [sesion, setSesion] = useState(localStorage.sesion || '')
    const handleSesion = () => {
        api.post('/service.php', Usuario.iniciar_sesion)
        .then( done => done.data )
        .then( data => data.response )
        .then( response => response.success && response.data && setSesion(response.data.claveSesion))
        .catch( err => console.log('Colppy Usuario.iniciar_sesion', err))
    }
    useEffect(() => {
        localStorage.sesion = sesion
    }, [sesion])

    const [value, setValue] = useState({ fuzz: '' })
    const handleChange = ({ target }) => setValue( value => ({...value, [target.name]: target.value }))

    const [provee, setProvee] = useState({})
    const handleSearch = () => {
        const { parameters } = Proveedor.listar_proveedor
        parameters.sesion.claveSesion = sesion
        parameters.filter[0].value = value.fuzz
        parameters.filter[1].value = value.fuzz

        api.post('/service.php', {...Proveedor.listar_proveedor, parameters })
        .catch( err => console.log('Colppy Proveedor.listar_proveedor', err))
        .then( done => done.data )
        .then( data => data.response.success && data.response.data )
        .then( data => data.map( m => 
            ['idProveedor', 'RazonSocial', 'NombreFantasia'].reduce((r, c) => ({...r, [c]: m[c] }), {})
        ))
        .then( data => setProvee(data) )
    }

    return (
        <div>
            <button onClick={ handleSesion }>Sesión</button>
            <pre>{ JSON.stringify(sesion, 0, 4) }</pre>
            <hr />
            <input name="fuzz" value={ value.fuzz } onChange={ handleChange } />
            <button onClick={ handleSearch }>Proveedores</button>
            <pre>{ JSON.stringify(provee, 0, 4) }</pre>
        </div>
    )
}