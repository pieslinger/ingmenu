import React, { useState, useEffect } from 'react'
import { Select } from 'grommet'

import useColppy from '../colppy'


export default function Typeahead({ initialValue, labelKey= 'RazonSocial', valueKey = 'idProveedor', service, onChange = ()=>{} }) {
    const { sesion, onSearch } = useColppy()

    const [options, setOptions] = useState([])
    const [value, setValue] = useState({ [valueKey]: undefined, [labelKey]: undefined })
    useEffect(() => {
        if (initialValue && initialValue[valueKey]) {
            setOptions([initialValue])
            setValue(initialValue)
        }
    }, [initialValue, valueKey])

    const handleSearch = (typeahead) => {
        if (! typeahead) return setOptions([])

        onSearch(typeahead)
        .then( list => setOptions(list))
    }
    const handleChange = ({ value }) => { setValue(value); onChange(value) }

    return (
        <div>
            <Select options={ options } value={ value } title={ sesion }
                valueKey={ valueKey } labelKey={ labelKey } searchPlaceholder="Buscar..."
                onSearch={ handleSearch }
                onChange={ handleChange }
            />
            <pre>{ JSON.stringify(value, 0, 4) }</pre>
        </div>
    )
}