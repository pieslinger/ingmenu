import React, { createContext, useContext, useState, useEffect } from 'react'
import dayjs from 'dayjs'

import { useLoginContext } from 'LoginContext'
import { rest } from 'api'


const Context = createContext()


export const useComprasContext = () => useContext(Context)
export default function ContextProvider({ service, children }) {
    const [id,   getItem] = useState(0)
    const [item, setItem] = useState({ id: 0, fecha: dayjs().format('YYYY-MM-DD'), list: [] })
    useEffect(() => {
        if (id) {
            rest('get', `${service}/${id}`).then( item => setItem(item))
        } else {
            setItem({ id: 0, fecha: dayjs().format('YYYY-MM-DD'), list: [] })
        }
    }, [service, id])

    const total = item.list.reduce((r, c) => (r + c.costo), 0)

    const { login } = useLoginContext()

    const handles = {
        handleCreate: (item) => rest('create', `${service}`,            {...item, id_admin: login.id }),
        handleUpdate: (item) => rest('update', `${service}/${item.id}`, {...item, id_admin: login.id })
            .then( data => { getItem(0); return data }),
    }
	return <Context.Provider value={{ item, getItem, total, ...handles }} children={ children } />
}
