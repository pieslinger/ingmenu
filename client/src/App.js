import React from 'react'
import { Router } from '@reach/router'

import { useLoginContext } from 'LoginContext'

import Login from 'Login'
import Menu from 'Menu'

import Compras from 'Compras'
import Colppy from 'Compras/Colppy'

import Insumos from 'Insumos'
import Consumos from 'Consumos'

import ProductosContextProvider from 'Productos/Context'
import Recetas from 'Recetas'
import Ventas  from 'Ventas'

import Dashboard  from 'Dashboard'


export default function App() {
	const { login } = useLoginContext()
	
	if (! login) return (
		<Router primary="false">
			<Login path="*" />
		</Router>
	)
	return (
		<ProductosContextProvider>
			<Router>
				<Menu path="/">
					<Compras path="compras/*" />
					<Colppy path="colppy" />

					<Insumos path="insumos/*" />
					<Consumos path="consumos/*" />
					
					<Recetas path="recetas/*" />
					<Ventas path="ventas/*" />

					<Dashboard default />
				</Menu>
			</Router>
		</ProductosContextProvider>
	)
}
