import React, { useEffect } from 'react'

import { useRecetasContext } from './Context'

import EditForm from './EditForm'


export default function Update({ navigate, id }) {
    const { item, getItem, handleUpdate } = useRecetasContext()

    useEffect(() => getItem(id), [getItem, id])
    
    const handleSubmit = (value) => handleUpdate(value)
    .then( result => console.log('Submitted', result, navigate(`/recetas`)))
    
    return <EditForm title={ item.id } item={ item } onSubmit={ handleSubmit } />
}
