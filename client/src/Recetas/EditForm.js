import React, { useState, useEffect } from 'react'
import { Grid, Box, FormField, Select, TextInput, Button } from 'grommet'

import { useIsDirty } from 'useIsDirty'
import { useProductosContext } from 'Productos/Context'

import Insumos from './Insumos'


export default function EditForm({ item, onSubmit }) {
    const [isDirty, setIsDirty] = useIsDirty()
    const [value, setValue] = useState({ id_categoria: null })
    useEffect(() => setValue(item), [item])

    const { options } = useProductosContext()

    const [option, setOption] = useState({value: 0, label: '', disabled: true })
    useEffect(() => {
        item.id_categoria && options.length &&
        setOption(options.find( f => f.value===item.id_categoria )) //  initialValue
    }, [item, options])

    if (! value || ! options) return null
    
    const handleChange = ({ target, option }) => {
        setIsDirty()
        if (option) {
            setOption(option)   //  ===event.value
            setValue({...value, [target.name]: option[target.name] })
        } else {
            setValue({...value, [target.name]: target.value })
        }
    }
    const handleReset  = () => { setIsDirty(false); setValue(item)  }
    const handleSubmit = () => { setIsDirty(false); onSubmit(value) }

    return (
        <Box pad="medium" animation="fadeIn">
            <TextInput type="text" name="producto" value={ value.producto || '' } plain readOnly />

            <Grid columns={["1/3", "2/3"]} rows="xxsmall">
                <FormField label="Categoria" />
                <Select name="id_categoria" value={ option } 
                    options={ options || [] } valueKey="value" labelKey="label" disabledKey="disabled"
                    onChange={ handleChange } required 
                />

                <FormField label="Producción" />
                <TextInput name="produccion" value={ value.produccion || 0 } min="1" onChange={ handleChange } required />
            </Grid>
            <br />
            <Insumos list={ value.list } onChange={ handleChange } />
            
            <Box justify="between" direction="row">
                <Button label="Reset"    onClick={ handleReset  } />
                <Button label="Confirma" onClick={ handleSubmit } disabled={! isDirty } primary />
            </Box>
        </Box>
	)
}
