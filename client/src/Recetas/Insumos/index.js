import React, { useState } from 'react'
import { Text, Box, DataTable, Button } from 'grommet'
import { AddCircle } from 'grommet-icons'

import { useThemeContext } from 'ThemeContext'
import { useRecetasContext } from 'Recetas/Context'

import EditForm from './EditForm'


export default function Insumos({ list, onChange }) {
    //  DataTable
    const { background } = useThemeContext()
    const columns = [
        { property: 'insumo',   header: 'Insumo', 
            render: ({ costo_receta, insumo }) => <Text color={ costo_receta ? "" : "status-critical" }>{ insumo }</Text>,
        },
        { property: 'cantidad', header: 'Cantidad', align: 'end' },
        { property: 'unidades', header: '' },
        { property: 'costo_receta', header: 'Costo', align: 'end', render: ({ costo_receta }) => costo_receta.toFixed(2) },
    ]
    const { item:{ id: id_producto }} = useRecetasContext()
    const [item, setItem] = useState(null)  //  Detail
    //  Form
    const handleSubmit = (value) => {
        setItem(null)
        
        if (value) {
            const target = { name: 'list' }
            const cleanup = ['cantidad'].reduce((r, k) => ({...r, [k]: Number(value[k]) }), {})

            Object.assign(value, cleanup)

            target.value = list.filter( f => f.id_insumo!==value.id_insumo )
            if (value.cantidad > 0) {
                target.value.push(value)
            }
            onChange({ target })
        }
        return value
    } 
    //  List
    const handleClickAdd = () => setItem({ id_producto, id_insumo: 0, cantidad: 0, unidades: '', costo: 0 })
    const handleClickRow = ({ datum }) => setItem(datum)

    if (item)
        return <EditForm item={ item } onSubmit={ handleSubmit } />
    else
        return (
            <Box background="light-1" direction="column">
                <DataTable alignSelf="stretch" background={ background } size="medium" 
                    columns={ columns } data={ list } 
                    onClickRow={ handleClickRow } 
                />
                <div>&nbsp;<Button icon={<AddCircle />} plain onClick={ handleClickAdd } /></div>
            </Box>
        )
}
