import React from 'react'
import { Router } from '@reach/router'
import { Box, Text } from 'grommet'

import ContextProvider from './Context'

import Browse from './Browse'
import Update from './Update'


export default (props) => {

    return (
        <ContextProvider>
            <Box as="header" align="center" background="neutral-4" pad="small">
                <Text weight="bold">Productos y Recetas</Text>
            </Box>

            <Router>
                <Browse path="/" />
                <Update path=":id" />
            </Router>
        </ContextProvider>
    )
}