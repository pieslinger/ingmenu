import React, { useState, useEffect } from 'react'
import { Accordion, AccordionPanel, Box, Text, DataTable } from 'grommet'
import { navigate } from '@reach/router'

import { rest } from 'api'
import { useProductosContext } from 'Productos/Context'
import { useThemeContext } from 'ThemeContext'


export default function Browse() {
    const [list, setList] = useState([])
    useEffect(() => { rest('find', 'productos').then( list => setList(list)); }, [])

    const { cats } = useProductosContext()
    const [sections, setSections] = useState([])
    useEffect(() => {
        const recursiveSections = (pid = null) => {    // null
            cats.length && list.length &&
            cats.filter( f => f.pid===pid ).map( m => {
                setSections( sections => ([...sections, { header: m.categoria, body: list.filter( f => f.id_categoria===m.id ) }]))
    
                return recursiveSections(m.id)
            })
        }
        recursiveSections()
    }, [cats, list])

    //  DataTable
    const { background } = useThemeContext()
    const columns = [
        { property: 'id',   header: 'Producto', 
            render: ({ costo_producto, producto }) => <Text color={ costo_producto ? "" : "status-critical"}>{ producto }</Text>
        },
        { property: 'costo_producto', header: 'Costo', align: 'end', 
            render: ({ costo_producto }) => costo_producto.toFixed(2) 
        },
        { property: 'costos_faltantes', header: 'Faltantes', align: 'end' }
    ]
    const handleClickRow = ({ datum: item }) => navigate(`/recetas/${item.id}`)

    return (
        <Accordion>
            { sections.map( m => {
                if (m.body.length===0) return <Box key={ m.header } pad={{ vertical: "small" }}><Text color="neutral-4" weight="bold">{ m.header }</Text></Box>

                return (
                    <AccordionPanel key={ m.header } label={ m.header }>
                        <DataTable alignSelf="stretch" background={ background } size="medium" 
                            columns={ columns } data={ m.body } sortable
                            onClickRow={ handleClickRow } 
                        />
                    </AccordionPanel>
                )
            }) }
        </Accordion>
	)
}
