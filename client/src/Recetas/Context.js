import React, { createContext, useContext, useState, useEffect } from 'react'

import { useLoginContext } from 'LoginContext'
import { rest } from 'api'


const Context = createContext()


export const useRecetasContext = () => useContext(Context)
export default function ContextProvider({ service = 'productos', children }) {
    const { login } = useLoginContext()

    const [cats, setCats] = useState([])
    useEffect(() => { rest('find', 'categorias').then( cats => setCats(cats)); }, [])

    const [id,   getItem] = useState(0)
    const [item, setItem] = useState({ recetas: [] })
    useEffect(() => {
        if (id) {
            rest('get', `${service}/${id}`).then( item => setItem(item))
        }
    }, [service, id])

    const total = 0 //  item.list.reduce((r, c) => (r + c.costo), 0)

    const handles = {
        handleUpdate: (item) => {
            const method = item.produccion > 0 ? 'update' : 'delete'

            return rest(method, `${service}/${item.id}`, {...item, id_admin: login.id })
                .then( data => { getItem(0); return data })
        } 
    }
	return <Context.Provider value={{ cats, item, getItem, total, ...handles }} children={ children } />
}
