import React, { createContext, useContext, useState, useEffect } from 'react'

import { rest } from 'api'


const Context = createContext()


export default function ContextProvider({ service = 'productos', children }) {
    const [cats, setCats] = useState([])
    useEffect(() => rest('find', 'categorias').then( cats => setCats(cats)) && undefined, [])

    const [options, setOptions] = useState([])
    useEffect(() => {
        const recursiveOptions = (pid = null) => {    // null
            cats.filter( f => f.pid===pid ).map( m => {
                setOptions( options => ([...options, { value: m.id, label: m.categoria, disabled: m.pid===null }]))
    
                return recursiveOptions(m.id)
            })
        }
        recursiveOptions()
    }, [cats])

    return <Context.Provider value={{ cats, options }} children={ children } />
}
export const useProductosContext = () => useContext(Context)
