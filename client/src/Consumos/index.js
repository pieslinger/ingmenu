import React from 'react'
import { Router } from '@reach/router'

import Daily  from './Daily'
import Weekly from './Weekly'


export default () => (
    <Router>
        <Weekly path=":date/weekly" />
        <Daily  path=":date" />
        <Daily  default />
    </Router>
)
