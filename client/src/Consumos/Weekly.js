import React, { useState, useEffect } from 'react'
import dayjs from 'dayjs'
import { Box, DataTable, Text } from 'grommet'
import { Up, Previous, Next, Bookmark } from 'grommet-icons'
import { Link } from '@reach/router'

import { rest } from 'api'
import { useThemeContext } from 'ThemeContext'


export default function Weekly({ date = dayjs().format('YYYY-MM-DD') }) {
    const from = dayjs(date).add(-6, 'day').format('YYYY-MM-DD')
    const [list, setList] = useState([])
    useEffect(() => {        
        date.match(/20\d\d-[0-1]\d-[0-3]\d/) &&
        rest('find', 'consumos', { from, thru: date })
        .then( list => Array.isArray(list) ? list : [list])
        .then( list => list.map( m => ({...m, costo: m.consumo * m.costo_unitario })))
        .then( list => setList(list))
    }, [date, from])

    const prevDate = dayjs(date).add(-7, 'day').format('YYYY-MM-DD')
    const nextDate = dayjs(date).add(+7, 'day').format('YYYY-MM-DD')

    //  DataTable
    const { background } = useThemeContext()
    const columns = [
        { property: 'insumo',  header: 'Insumo' },
        { property: 'consumo', header: 'Consumo', align: 'end' },
        { property: 'unidades',header: '' },
        { property: 'costo',   header: 'Costo', render: (item) => item.costo.toFixed(2),
            footer: { aggregate: true }, aggregate: 'sum', align: 'end' 
        },
    ]
    
    return (
        <Box background="light-1" pad="medium">
            <Text alignSelf="center" weight="bold">Consumos</Text>
                
            <Box as="header" pad="medium" justify="between" direction="row">
                <Link to={`/`}><Up /></Link>
                <Link to={`../${prevDate}`}><Previous /></Link>

                <Text weight="bold">{ dayjs(from).format('MMM.DD') } a { dayjs(date).format('MMM.DD') }</Text>
                
                <Link to={`../${nextDate}`}><Next /></Link>
                <Bookmark />
            </Box>

            <Box>
                <DataTable alignSelf="stretch" background={ background } size="medium" 
                    columns={ columns } data={ list } 
                />
            </Box>
        </Box>
	)
}
