import React, { createContext, useState, useContext } from 'react'

import { rest } from 'api'


const Context = createContext()


export const useLoginContext = () => useContext(Context)
export default function ContextProvider({ children }) {
    const [login, setLogin] = useState(JSON.parse(sessionStorage.login || "null"))

    const handleLogin = async ({ value }) => {
        const data = await rest('find', 'admins', value)
        .then( list => list && list.length ? list[0] : null )
        
        if (data) {
            sessionStorage.login = JSON.stringify(data)
            setLogin(data)
            return true
        }
        return false
    }
    const handleLogout = () => {
        sessionStorage.login = null
        setLogin(null)
        return true
    }
	return <Context.Provider value={{ login,  handleLogin, handleLogout }} children={ children } />
}
