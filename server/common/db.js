const feathers = require('@feathersjs/feathers');
const configuration = require('@feathersjs/configuration');

// Use the application root and `config/` as the configuration folder
const MYSQL = feathers().configure(configuration()).get('mysql')

const db = require('mysql2').createPool({
    connectionLimit: 10,
    host : MYSQL.host,
    user : MYSQL.user,
    password : MYSQL.password,
    database : MYSQL.database
})
db.query = require('util').promisify(db.query)

module.exports = db
//
//  Utils
//
const log = async (id, entity) => {
	return await db.query(`
		INSERT IGNORE _logs (id, entity, log)
		VALUES (${id}, '${entity}', JSON_ARRAY())
	`).then(() => db.query(`
		SELECT * FROM ${entity} WHERE id = ${id}
	`)).then( list => db.query(`
		UPDATE _logs
		SET log = JSON_ARRAY_APPEND(log, '$', CAST('${JSON.stringify(list[0])}' AS JSON))
		WHERE id = ${id} AND entity = '${entity}'
	`))
}
const fieldNames = async (entity) => await db.query(`DESCRIBE ${entity}`)
.then( TextRow => Array.from(TextRow).map( m => m.Field ))

const enums = async (entity) => await db.query(`SHOW COLUMNS FROM ${entity} WHERE type LIKE 'enum(%'`)
// .then( TextRow => Array.from(TextRow).reduce((r, c) => ({...r, [c.Field]: c.Type.match(/'(.*?)'/g) }), {}))
.then( TextRow => Array.from(TextRow))
.then( list => list.reduce((r, c) => ({...r,
	[c.Field]: c.Type.match(/'(.*?)'/g).map( m => m.replace(/'/g, "")) 
}), {}) )

const cleanup = async (data, entity) => {
	const fields = await fieldNames(entity)
    
	return fields.filter( k => data[k] ).reduce((r, k) => ({...r, [k]: data[k] }), {})
}

Object.assign(module.exports, { log, fieldNames, enums, cleanup })
