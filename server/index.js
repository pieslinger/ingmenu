/* eslint-disable no-console */
const logger = require('./logger');
const app = require('./app');
const port = process.env.NODE_PORT;

// console.log(process.env)

let server = null

if (process.env.NODE_ENV!=='production') {
  server = app.listen(port);
}
else {
  const fs = require('fs');
  const https = require('https');
  const opts = {
    cert: fs.readFileSync(process.env.SSL_CERT),
    key: fs.readFileSync(process.env.SSL_KEY),
  }
  server = https.createServer(opts, app).listen(port);
  // magic sauce! Socket w/ ssl
  app.setup(server);
}

process.on('unhandledRejection', (reason, p) =>
  logger.error('Unhandled Rejection at: Promise ', p, reason)
);

server.on('listening', () =>
  logger.info('Feathers application started on port %d', port)
);
