// Initializes the `consumos` service on path `/consumos`
const { Consumos } = require('./consumos.class');
const hooks = require('./consumos.hooks');

module.exports = function (app) {
  
  const paginate = app.get('paginate');

  const options = {
    paginate
  };

  // Initialize our service with any options it requires
  app.use('/consumos', new Consumos(options, app));

  // Get our initialized service so that we can register hooks
  const service = app.service('consumos');

  service.hooks(hooks);
};
