const db = require('../../common/db.js')

/* eslint-disable no-unused-vars */
exports.Consumos = class Consumos {
  constructor (options) {
    this.options = options || {};
  }

	async find ({ query }) {
    const from = query.from || query.date
    const thru = query.thru || query.date

    if (from && thru) return await db.query(`
      SELECT I.*, I.compra AS unidades, C.costo_unitario
      , ROUND(SUM(Q.cantidad * IFNULL(P.produccion, 1) * R.cantidad)/ I.factor, 3) AS consumo
      FROM ventas V
      JOIN ventas_productos Q ON V.id = Q.id_venta

      JOIN productos P ON Q.id_producto = P.id 
      JOIN recetas R ON P.id = R.id_producto
      JOIN insumos I ON R.id_insumo = I.id
      LEFT JOIN (
        SELECT C.*, ROUND(costo / cantidad, 2) AS costo_unitario
        FROM (
            SELECT id_insumo, MAX(id_compra) AS id_compra 
            FROM compras_insumos
            GROUP BY id_insumo
        ) X
        JOIN compras_insumos C ON X.id_insumo = C.id_insumo AND X.id_compra = C.id_compra
      ) C ON I.id = C.id_insumo
      
      WHERE V.fecha BETWEEN '${from}' AND '${thru}'
      GROUP BY I.insumo 
      ORDER BY I.insumo
    `)
    return [{ error: 'Missing date / date range'}]
	}

  async get (id, params) {
    return {
      id, text: `A new message with ID: ${id}!`
    };
  }

  async create (data, params) {
    if (Array.isArray(data)) {
      return Promise.all(data.map(current => this.create(current, params)));
    }

    return data;
  }

  async update (id, data, params) {
    return data;
  }

  async patch (id, data, params) {
    return data;
  }

  async remove (id, params) {
    return { id };
  }
}
