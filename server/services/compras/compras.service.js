// Initializes the `compras` service on path `/compras`
const createService = require('./compras.class.js');
const hooks = require('./compras.hooks');

module.exports = function (app) {
  
  const paginate = app.get('paginate');

  const options = {
    paginate
  };

  // Initialize our service with any options it requires
  app.use('/compras', createService(options));

  // Get our initialized service so that we can register hooks
  const service = app.service('compras');

  service.hooks(hooks);
};
