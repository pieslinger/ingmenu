const createProveedor = async (context) => {
	const { app, data } = context

	await app.service('proveedores')
	.create({ idProveedor: data.id_proveedor, RazonSocial: data.RazonSocial })
	.then( result => console.log('Nuevo proveedor', result))
	.catch( err => console.log('ERROR en Nuevo Proveedor'))

	return context
}

const updateInsumos = async (context) => {
	const { app, data, method, result } = context
	const id = (method=='create' ? result.id : data.id)

	result.list = await app.service('compras-insumos').update(null, data.list.map( m => ({...m, id_compra: id })))

	return {...context, result }
}

const fixDateFormat = context => {
	const { result } = context
	const fixIt = item => item.fecha ? {...item, fecha: item.fecha.toJSON().slice(0, 10) } : item

	return {...context, result: Array.isArray(result) ? result.map( m => fixIt(m)) : fixIt(result) }
}

const fixTotalNumber = context => {
	const { result } = context
	const fixIt = item => item.total ? {...item, total: Number(item.total) } : item

	return {...context, result: Array.isArray(result) ? result.map( m => fixIt(m)) : fixIt(result) }
}

module.exports = {
	before: {
		all: [],
		find: [],
		get: [],
		create: [createProveedor],
		update: [createProveedor],
		patch: [],
		remove: [updateInsumos]
	},

	after: {
		all: [],
		find: [fixDateFormat, fixTotalNumber],
		get: [fixDateFormat, fixTotalNumber],
		create: [updateInsumos],
		update: [updateInsumos],
		patch: [],
		remove: []
	},

	error: {
		all: [],
		find: [],
		get: [],
		create: [],
		update: [],
		patch: [],
		remove: []
	}
};
