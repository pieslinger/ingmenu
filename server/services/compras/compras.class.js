const db = require('../../common/db.js')
const { log, cleanup } = require('../../common/db.js')

/* eslint-disable no-unused-vars */
class Service {
	constructor (options) {
		this.options = options || {};
	}

	setup (app) {
		this.app = app
	}

	async find ({ query }) {
		if (query.id_insumo) return await db.query(`
			SELECT C.*, P.RazonSocial
			, IFNULL(SUM(I.costo), 0) AS total
			FROM compras C
			JOIN proveedores P ON C.id_proveedor = P.idProveedor
			LEFT JOIN compras_insumos I ON C.id = I.id_compra
			WHERE I.id_insumo = '${query.id_insumo}'
			GROUP BY C.id_proveedor
		`)
		if (query.id_proveedor) return await db.query(`
			SELECT C.*, P.RazonSocial
			, IFNULL(SUM(I.costo), 0) AS total
			FROM compras C
			JOIN proveedores P ON C.id_proveedor = P.idProveedor
			LEFT JOIN compras_insumos I ON C.id = I.id_compra
			WHERE C.id_proveedor = '${query.id_proveedor}'
			GROUP BY C.id_proveedor
		`)
		if (query.from && query.thru) return await db.query(`
			SELECT C.fecha
			, IFNULL(SUM(I.costo), 0) AS total
			FROM compras C
			LEFT JOIN compras_insumos I ON C.id = I.id_compra
			WHERE C.fecha BETWEEN '${query.from}' AND '${query.thru}'
			GROUP BY C.fecha
		`)
		if (query.date) return await db.query(`
			SELECT C.*, P.RazonSocial, A.admin
			, GROUP_CONCAT(I.id_insumo SEPARATOR ',') AS insumos
			, IFNULL(SUM(I.costo), 0) AS total
			FROM compras C
			JOIN proveedores P ON C.id_proveedor = P.idProveedor
			LEFT JOIN admins A ON C.id_admin = A.id
			LEFT JOIN compras_insumos I ON C.id = I.id_compra
			WHERE C.fecha = '${query.date}'
			GROUP BY C.id
		`)
		return [{ error: 'Missing parameters', query }]
	}

	async get (id, params) {
		const item = await db.query(`
			SELECT C.*, P.RazonSocial, A.login AS admin
			, SUM(I.costo) AS total
			FROM compras C
			JOIN proveedores P ON C.id_proveedor = P.idProveedor
			LEFT JOIN admins A ON C.id_admin = A.id
			LEFT JOIN compras_insumos I ON C.id = I.id_compra
			WHERE C.id = ${id}
		`)
		.then( list => list && list.length ? list[0] : {})

		item.list = await this.app.service('compras-insumos').find({ query:{ id_compra: id }})
		
		return item
	}

	async create (data, params) {
		const compra = await cleanup(data, 'compras')
		const result = await db.query(`INSERT INTO compras SET ?`, compra)
		
		return {...compra, id: result.insertId, result } || {}
	}

	async update (id, data, params) {
		data.id = Number(id)
		console.log('logging id', id, await log(id, 'compras'))

		const compra = await cleanup(data, 'compras')
		const sql = db.format(`UPDATE compras SET ? WHERE id = ?`, [compra, id])
		console.log('compras.update', sql)
		const result = await db.query(sql)
		.catch( err => console.log(`compras.update(${id}) error in UPDATE`, err, compra))

		return {...compra, result } || {}
	}

	async patch (id, data, params) {
		return data;
	}

	async remove (id, params) {
		return { id };
	}
}

module.exports = function (options) {
	return new Service(options);
};

module.exports.Service = Service;
