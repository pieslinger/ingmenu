const db = require('../../common/db.js')
const { log, cleanup } = require('../../common/db.js')

/* eslint-disable no-unused-vars */
class Service {
	constructor (options) {
		this.options = options || {};
	}

	async find ({ query }) {
		if (query.from && query.thru) return await db.query(`
			SELECT V.fecha, SUM(D.cantidad) AS total
			, GROUP_CONCAT(DISTINCT C.categoria SEPARATOR ' / ') AS vouchers
			FROM ventas V
			LEFT JOIN ventas_productos D ON V.id = D.id_venta
			LEFT JOIN productos_vue P ON D.id_producto = P.id
			LEFT JOIN categorias C ON P.pid_categoria = C.id
			WHERE V.fecha BETWEEN '${query.from}' AND '${query.thru}'
			GROUP BY V.fecha
		`)
		if (query.date) return await db.query(`
			SELECT V.*, A.admin, SUM(D.cantidad) AS cantidad
			, C.categoria, T.categoria AS tipo
			FROM ventas V
			LEFT JOIN admins A ON V.id_admin = A.id
			LEFT JOIN ventas_productos D ON V.id = D.id_venta
			LEFT JOIN productos_vue P ON D.id_producto = P.id
			LEFT JOIN categorias C ON P.pid_categoria = C.id
			LEFT JOIN categorias T ON P.id_categoria = T.id
			WHERE V.fecha = '${query.date}'
			GROUP BY C.categoria, T.categoria
		`)
		return [{ error: 'Missing parameters', query }]
	}

	async get (id, params) {
		return await db.query(`
			SELECT V.*, A.admin
			FROM ventas V
			LEFT JOIN admins A ON V.id_admin = A.id
			WHERE V.id = ${id}
		`)
		.then( list => list && list.length ? list[0] : {})
	}

	async create (data, params) {
		const venta = await cleanup(data, 'ventas')
		const result = await db.query(`INSERT INTO ventas SET ?`, venta)
		
		return {...venta, id: result.insertId, result } || {}
	}

	async update (id, data, params) {
		// id = Number(id)
		console.log('logging id', id, await log(id, 'ventas'))

		const venta = await cleanup(data, 'ventas')
		const sql = db.format(`UPDATE ventas SET ? WHERE id = ?`, [venta, id])
		console.log('ventas.update', sql)
		const result = await db.query(sql)
		.catch( err => console.log(`ventas.update(${id}) error in UPDATE`, err, venta))

		return {...venta, result } || {}
	}

	async patch (id, data, params) {
		return data;
	}

	async remove (id, params) {
		return { id };
	}
}

module.exports = function (options) {
	return new Service(options);
};

module.exports.Service = Service;
