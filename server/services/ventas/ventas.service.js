// Initializes the `ventas` service on path `/ventas`
const createService = require('./ventas.class.js');
const hooks = require('./ventas.hooks');

module.exports = function (app) {
  
  const paginate = app.get('paginate');

  const options = {
    paginate
  };

  // Initialize our service with any options it requires
  app.use('/ventas', createService(options));

  // Get our initialized service so that we can register hooks
  const service = app.service('ventas');

  service.hooks(hooks);
};
