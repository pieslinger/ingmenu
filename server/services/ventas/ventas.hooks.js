const fixIt = (item) => {
  if (item.fecha)    Object.assign(item, { fecha: item.fecha.toJSON().slice(0, 10) })
  if (item.cantidad) Object.assign(item, { cantidad: Number(item.cantidad) })
  if (item.total)    Object.assign(item, { total: Number(item.total) })

  return item
} 

const fixFormats = (context) => {
	const { result } = context

	return ({...context, result: Array.isArray(result) ? result.map( m => fixIt(m)) : fixIt(result) })
}

const getProductos = async (context) => {
	const { app, data, result } = context
	const id = result && result.id ? result.id : 0
console.log('getProductos data', data)
  result.list = await app.service('ventas-productos').find({ query:{ id_venta: id }})

	return {...context, result }
}

const updateProductos = async (context) => {
	const { app, data, method, result } = context
	const id = (method=='create' ? result.id : data.id)

	result.list = await app.service('ventas-productos').update(null, data.list.map( m => ({...m, id_venta: id })))

	return {...context, result }
}


module.exports = {
  before: {
    all: [],
    find: [],
    get: [],
    create: [],
    update: [],
    patch: [],
    remove: [updateProductos]
  },

  after: {
    all: [],
    find: [fixFormats],
    get: [fixFormats, getProductos],
    create: [updateProductos],
    update: [updateProductos],
    patch: [],
    remove: []
  },

  error: {
    all: [],
    find: [],
    get: [],
    create: [],
    update: [],
    patch: [],
    remove: []
  }
};
