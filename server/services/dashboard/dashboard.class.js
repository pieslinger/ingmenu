const { MySql } = require('../mysql.class')

/* eslint-disable no-unused-vars */
exports.Dashboard = class Dashboard extends MySql {
  constructor (options) {
		super (options)

    this.options = options || {};
  }

  async find (params) {
    const compras = this.db.query(`
      SELECT max_fecha, SUM(costo) AS sum_costo
      FROM (
        SELECT MAX(fecha) AS max_fecha FROM compras
      ) max_compras 
      JOIN compras ON max_fecha = fecha
      JOIN compras_insumos ON id = id_compra
    `).then( list => list.length ? list[0] : {})
    
    const insumos = this.db.query(`
      SELECT COUNT(*) AS count_total, SUM(
        IF(compra IS NULL OR receta IS NULL OR factor = 0, 0, 1)
      ) AS count_ok
      FROM insumos
    `).then( list => list.length ? list[0] : {})

    const ventas = this.db.query(`
      SELECT max_fecha, SUM(cantidad) AS sum_cantidad
      FROM (
        SELECT MAX(fecha) AS max_fecha FROM ventas
      ) max_ventas 
      JOIN ventas ON max_fecha = fecha
      JOIN ventas_productos ON id = id_venta
    `).then( list => list.length ? list[0] : {})
    
    return await Promise.all([compras, insumos, ventas])
    .then(([compras, insumos, ventas]) => ({ compras, insumos, ventas }))
  }

  async get (id, params) {
    return {
      id, text: `A new message with ID: ${id}!`
    };
  }

  async create (data, params) {
    return data;
  }

  async update (id, data, params) {
    return data;
  }

  async patch (id, data, params) {
    return data;
  }

  async remove (id, params) {
    return { id };
  }
}
