const fixDataTypes = (context) => {
  const { result } = context

  result.compras.max_fecha = result.compras.max_fecha.toJSON().slice(0, 10)
  result.compras.sum_costo = Number(result.compras.sum_costo)
  result.insumos.count_ok = Number(result.insumos.count_ok)
  result.ventas.max_fecha = result.ventas.max_fecha.toJSON().slice(0, 10)
  result.ventas.sum_cantidad = Number(result.ventas.sum_cantidad)

  return {...context, result }
}

module.exports = {
  before: {
    all: [],
    find: [],
    get: [],
    create: [],
    update: [],
    patch: [],
    remove: []
  },

  after: {
    all: [],
    find: [fixDataTypes],
    get: [],
    create: [],
    update: [],
    patch: [],
    remove: []
  },

  error: {
    all: [],
    find: [],
    get: [],
    create: [],
    update: [],
    patch: [],
    remove: []
  }
};
