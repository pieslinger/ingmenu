const api = require('axios').create({ 
  baseURL: 'https://login.colppy.com/lib/frontera2' 
  // baseURL: 'https://staging.colppy.com/lib/frontera2' 
})

const Usuario = {
  iniciar_sesion: {
    "service": {
      "provision": "Usuario",
      "operacion": "iniciar_sesion"
    },
    "auth": {
      "usuario": "klaus@pieslinger.net",
      "password": "fb2f020d9356e813822757e15eb2f6d9"
    },
    "parameters": {
      "usuario": "klaus@pieslinger.net",
      "password": "39bb37cf36d3b29a9280d8a70a0eed42"
    }
  }
}
const Proveedor = {
  listar_proveedor: {
    "auth": {
      "usuario": "klaus@pieslinger.net",
      "password": "fb2f020d9356e813822757e15eb2f6d9"
    },
    "service": {
      "provision": "Proveedor",
      "operacion": "listar_proveedor"
    },
    "parameters": {
      "sesion": {
        "usuario": "klaus@pieslinger.net",
        "claveSesion": "-- tbc --"
      },
      "idEmpresa": "5776",
      "start": 0,
      "limit": 50,
      "order":[{
        "field":"NombreFantasia",
        "order":"ASC"
      }],
      "filter": [{
        "field":"Activo",
        "op":"=",
        "value":"1"
      }]
    }
  }
}
/* eslint-disable no-unused-vars */
class Service {
  constructor (options) {
    this.options = options || {};
  }

  async find ({ query }) {
    console.log('query', query)
    if (! query.op) return { error: 'Missing op'}

    if (query.op=='sesion') return await api.post('/service.php', Usuario.iniciar_sesion)
      .then( done => done.data )
      .then( data => data.response )
      .then( response => response.success && response.data )
      .catch( err => console.log('Colppy Usuario.iniciar_sesion', err))

    if (query.op=='proveedores' && query.sesion) {
      Proveedor.listar_proveedor.parameters.sesion.claveSesion = query.sesion
      const myJSON = JSON.stringify(Proveedor.listar_proveedor)
      console.log('listar_proveedor', myJSON)

      return await api.post('/service.php', JSON.parse(myJSON))
        .catch( err => console.log('Colppy Proveedor.listar_proveedor', err))
        // .then( done => done.data )
        // .then( data => data.response )
        // .then( response => response.success && response.data )
    } 
    else 
    if (query.op=='proveedores') {
      const data = await this.find({ query:{ op: 'sesion' }})

      if (! data || ! data.claveSesion) return { error: data }

      Proveedor.listar_proveedor.parameters.sesion.claveSesion = data.claveSesion
      console.log(Proveedor.listar_proveedor)

      return await api.post('/service.php', Proveedor.listar_proveedor)
        // .then( done => done.data )
        // .then( data => data.response )
        // .then( response => response.success && response.data )
        // .catch( err => console.log('Colppy Proveedor.listar_proveedor', err))
    } 
  }


  async get (id, params) {
    return {
      id, text: `A new message with ID: ${id}!`
    };
  }

  async create (data, params) {
    if (Array.isArray(data)) {
      return Promise.all(data.map(current => this.create(current, params)));
    }

    return data;
  }

  async update (id, data, params) {
    return data;
  }

  async patch (id, data, params) {
    return data;
  }

  async remove (id, params) {
    return { id };
  }
}

module.exports = function (options) {
  return new Service(options);
};

module.exports.Service = Service;
