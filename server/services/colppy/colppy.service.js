// Initializes the `colppy` service on path `/colppy`
const createService = require('./colppy.class.js');
const hooks = require('./colppy.hooks');

module.exports = function (app) {
  
  const paginate = app.get('paginate');

  const options = {
    paginate
  };

  // Initialize our service with any options it requires
  app.use('/colppy', createService(options));

  // Get our initialized service so that we can register hooks
  const service = app.service('colppy');

  service.hooks(hooks);
};
