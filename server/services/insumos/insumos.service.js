// Initializes the `insumos` service on path `/insumos`
const createService = require('./insumos.class.js');
const hooks = require('./insumos.hooks');

module.exports = function (app) {
  
  const paginate = app.get('paginate');

  const options = {
    paginate
  };

  // Initialize our service with any options it requires
  app.use('/insumos', createService(options));

  // Get our initialized service so that we can register hooks
  const service = app.service('insumos');

  service.hooks(hooks);
};
