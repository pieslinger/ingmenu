const db = require('../../common/db.js')

db.query(`
  CREATE OR REPLACE VIEW insumos_vue
  AS
  SELECT I.*, costo_compra
  , IFNULL(cnt_compras, 0) AS cnt_compras
  , ROUND(costo_compra / factor, 2) AS costo_receta
  FROM insumos I
  LEFT JOIN (
    SELECT C.*, ROUND(costo / cantidad, 2) AS costo_compra, X.cnt_compras
    FROM (
      SELECT id_insumo, MAX(id_compra) AS id_compra, COUNT(*) AS cnt_compras
      FROM compras_insumos
      GROUP BY id_insumo
    ) X
    JOIN compras_insumos C 
      ON X.id_insumo = C.id_insumo AND X.id_compra = C.id_compra
  ) C ON I.id = C.id_insumo
`).then( result => console.log('insumos_vue:', JSON.stringify(result)))

/* eslint-disable no-unused-vars */
class Service {
  constructor (options) {
    this.options = options || {};
  }

	async find ({ query }) {
    if (query.fuzzy) return await db.query(`
      SELECT * FROM insumos_vue
      WHERE insumo LIKE '%${query.fuzzy}%'
      ORDER BY insumo
    `)
    
    return await db.query(`
      SELECT I.*, I.cnt_compras AS compras
      , COUNT(DISTINCT R.id_producto) AS recetas
      FROM insumos_vue I
      LEFT JOIN recetas R ON I.id = R.id_insumo
      GROUP BY I.id
      ORDER BY insumo
    `)
	}

  async get (id, params) {
    return await db.query(`
      SELECT I.*, I.cnt_compras AS compras
      , COUNT(DISTINCT R.id_producto) AS recetas
      FROM insumos_vue I
      LEFT JOIN recetas R ON I.id = R.id_insumo
      WHERE I.id = ${id}
    `)
		.then( list => list && list.length ? list[0] : {})
  }

  async create (data, params) {
    return {...data, error: 'Method not implemented' }
  }

  async update (id, data, params) {
    data = await db.cleanup(data, 'insumos')
    const sql = db.format(`UPDATE insumos SET ? WHERE id = ${id}`, data)
    console.log(sql)
    return await db.query(sql)
  }

  async patch (id, data, params) {
    return {...data, error: 'Method not implemented' }
  }

  async remove (id, params) {
    return { id, error: 'Method not implemented' };
  }
}

module.exports = function (options) {
  return new Service(options);
};

module.exports.Service = Service;
