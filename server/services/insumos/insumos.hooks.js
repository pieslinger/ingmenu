const fixDataTypes = (context) => ({...context, 
  result: context.result.map( m => { 
    m.costo_compra = Number(m.costo_compra) 
    m.costo_receta = Number(m.costo_receta) 

    return m
  })
})

module.exports = {
  before: {
    all: [],
    find: [],
    get: [],
    create: [],
    update: [],
    patch: [],
    remove: []
  },

  after: {
    all:  [],
    find: [fixDataTypes],
    get:  [],
    create: [],
    update: [],
    patch: [],
    remove: []
  },

  error: {
    all: [],
    find: [],
    get: [],
    create: [],
    update: [],
    patch: [],
    remove: []
  }
};
