// Initializes the `productos` service on path `/productos`
const createService = require('./productos.class.js');
const hooks = require('./productos.hooks');

module.exports = function (app) {
  
  const paginate = app.get('paginate');

  const options = {
    paginate
  };

  // Initialize our service with any options it requires
  app.use('/productos', createService(options));

  // Get our initialized service so that we can register hooks
  const service = app.service('productos');

  service.hooks(hooks);
};
