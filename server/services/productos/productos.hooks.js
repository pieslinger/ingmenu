const fixDataTypes = (context) => ({...context, 
  result: context.result.map( m => ({...m,
    costo_producto: Number(m.costo_producto), 
    costos_faltantes: Number(m.costos_faltantes), 
  }))
})

const findRecetas = async (context) => {
  const { app, result } = context
console.log(result.id)
  result.list = result.id ? (await app.service('recetas').find({ query:{ id_producto: result.id }})) : []

  return {...context, result }
}

const updateRecetas = async (context) => {
	const { app, data, method, result } = context
	const id_producto = (method=='create' ? result.id : data.id)

	await app.service('recetas').update(null, data.recetas.map( m => ({...m, id_producto })))

	result.recetas = await app.service('recetas').find({ query:{ id_producto }})

	return {...context, result }
}


module.exports = {
  before: {
    all: [],
    find: [],
    get: [],
    create: [],
    update: [],
    patch: [],
    remove: []
  },

  after: {
    all: [],
    find: [fixDataTypes],
    get:  [findRecetas],
    create: [updateRecetas],
    update: [updateRecetas],
    patch: [],
    remove: []
  },

  error: {
    all: [],
    find: [],
    get: [],
    create: [],
    update: [],
    patch: [],
    remove: []
  }
};
