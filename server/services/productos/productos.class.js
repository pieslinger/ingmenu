const db = require('../../common/db.js')
const { cleanup } = require('../../common/db.js')

db.query(`
	CREATE OR REPLACE VIEW productos_vue
	AS
	SELECT P.*, T.pid AS pid_categoria
	FROM productos P
	LEFT JOIN categorias T ON P.id_categoria = T.id
	LEFT JOIN categorias C ON T.pid = C.id
`).then( result => console.log('productos_vue:', JSON.stringify(result)))

/* eslint-disable no-unused-vars */
class Service {
	constructor (options) {
		this.options = options || {};
	}

	setup (app) {
		this.app = app
	}

	async find ({ query }) {
		if (query.enums) return await db.enums(`productos`)

		if (query.faltantes) return await db.query(`
			SELECT P.*, COUNT(R.id_insumo) AS insumos, COUNT(V.id_venta) AS ventas
			FROM productos P
			LEFT JOIN recetas_insumos R ON P.id = R.id_producto
			LEFT JOIN ventas_productos V ON P.id = V.id_producto
			GROUP BY P.id HAVING COUNT(R.id_insumo) = 0
		`)
		//	TODO: these 2 above are in use?

		return await db.query(`
			SELECT P.*
			, ROUND(SUM(I.costo_receta * R.cantidad)/ P.produccion, 2) AS costo_producto
			, SUM(IF(IFNULL(I.costo_receta, 0) = 0, 1, 0)) AS costos_faltantes
			FROM productos_vue P
			LEFT JOIN recetas R ON P.id = R.id_producto
			LEFT JOIN insumos_vue I ON R.id_insumo = I.id
			GROUP BY P.id
			ORDER BY P.tipo, P.orden
		`)
	}

	async get (id, params) {
		const item = await db.query(`
			SELECT * FROM productos 
			WHERE id = ${id}
		`)
		.then( list => list.length ? list[0] : { error: `Product #${id} not found` })

		item._enums = await db.enums('productos')
		
		return item
	}

	async create (data, params) {
		return data;
	}

	async update (id, data, params) {
		const producto = await cleanup(data, 'productos')

		return db.query(`UPDATE productos SET ? WHERE id = ?`, [producto, id])
		.then( result => ({...data, result }))
	}

	async patch (id, data, params) {
		return data;
	}

	async remove (id, params) {
		return await db.query(`DELETE FROM productos WHERE id = ${id}`)
		.catch( err => console.log(`productos.remove(${id}) error in DELETE`, err.sqlMessage))
	}
}

module.exports = function (options) {
  return new Service(options);
};

module.exports.Service = Service;
