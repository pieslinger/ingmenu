const db = require('../../common/db.js')
/*
CREATE OR REPLACE VIEW admins
AS
SELECT id_user AS id, login, email, Nombre AS admin
FROM cafe_rojo_0520.ac_usuarios
WHERE FIND_IN_SET('ingmenu', admin) > 0
;
*/
/* eslint-disable no-unused-vars */
class Service {
  constructor (options) {
    this.options = options || {};
  }

  async find ({ query }) {
    if (query.login) {
      return await db.query(`SELECT * FROM admins WHERE login = '${query.login}'`)
    }
    return []
  }

  async get (id, params) {
    return await db.query(`SELECT * FROM admins WHERE id = ${id}`)
    .then( list => list && list.length && list[0])
  }

  async create (data, params) {
    if (Array.isArray(data)) {
      return Promise.all(data.map(current => this.create(current, params)));
    }

    return data;
  }

  async update (id, data, params) {
    return data;
  }

  async patch (id, data, params) {
    return data;
  }

  async remove (id, params) {
    return { id };
  }
}

module.exports = function (options) {
  return new Service(options);
};

module.exports.Service = Service;
