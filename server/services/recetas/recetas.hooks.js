const fixDataTypes = (context) => ({...context, 
  result: context.result.map( m => ({...m,
    cantidad: Number(m.cantidad),
    costo_receta: Number(m.costo_receta), 
  }))
})

module.exports = {
  before: {
    all: [],
    find: [],
    get: [],
    create: [],
    update: [],
    patch: [],
    remove: []
  },

  after: {
    all:  [],
    find: [fixDataTypes],
    get:  [],
    create: [],
    update: [],
    patch:  [],
    remove: []
  },

  error: {
    all: [],
    find: [],
    get: [],
    create: [],
    update: [],
    patch: [],
    remove: []
  }
};
