// Initializes the `recetas` service on path `/recetas`
const createService = require('./recetas.class.js');
const hooks = require('./recetas.hooks');

module.exports = function (app) {
  
  const paginate = app.get('paginate');

  const options = {
    paginate
  };

  // Initialize our service with any options it requires
  app.use('/recetas', createService(options));

  // Get our initialized service so that we can register hooks
  const service = app.service('recetas');

  service.hooks(hooks);
};
