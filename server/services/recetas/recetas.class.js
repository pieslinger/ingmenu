const db = require('../../common/db.js')
const { cleanup } = require('../../common/db.js')

/* eslint-disable no-unused-vars */
class Service {
  constructor (options) {
    this.options = options || {};
  }

  async find ({ query }) {
    if (! query.id_producto) return [{ error: 'Missing parameter' }]
    
    //  TODO: does recetas.id have any sense?
    return await db.query(`
      SELECT R.*, I.insumo
      , I.costo_receta * R.cantidad AS costo_receta
			FROM recetas R
			LEFT JOIN insumos_vue I ON R.id_insumo = I.id
			WHERE R.id_producto = '${query.id_producto}'
    `)
  }

  async get (id, params) {
    return { id, params, error: 'unsupported method' }
  }

  async create (data, params) {
    return {...data, error: 'unsupported method' }
  }

  async update (id, data, params) {
    if (Array.isArray(data)) {
      return Promise.all(data.map( current => this.update(id, current, params)))
    }
    if (id===null) {  //  we don't use id here, we replace via unique keys
      if (data.id_insumo < 0) await db.query(`INSERT insumos SET ?`, { insumo: data.insumo })
      .then( result => { data.id_insumo = result.insertId })

      data = await cleanup(data, 'recetas')

      const result = await db.query(`REPLACE recetas SET ?`, data)

      return {...data, result }
    }
  }

  async patch (id, data, params) {
    return data;
  }

  async remove (id, params) {
    return { id };
  }
}

module.exports = function (options) {
  return new Service(options);
};

module.exports.Service = Service;
