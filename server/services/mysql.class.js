const feathers = require('@feathersjs/feathers');
const configuration = require('@feathersjs/configuration');

// Use the application root and `config/` as the configuration folder
const MYSQL = feathers().configure(configuration()).get('mysql')

const { createPool } = require('mysql2')
const connection = {
    connectionLimit: 10,
    host : MYSQL.host,
    user : MYSQL.user,
    password : MYSQL.password,
    database : MYSQL.database
}
const { promisify } = require('util')

exports.MySql = class MySql {
	constructor (options) {
		this.db = createPool(connection)
		this.db.query = promisify(this.db.query)
	}

	async log (id, entity) {
		return await this.db.query(`
			INSERT IGNORE _logs (id, entity, log)
			VALUES (${id}, '${entity}', JSON_ARRAY())
		`).then(() => this.db.query(`
			SELECT * FROM ${entity} WHERE id = ${id}
		`)).then( list => this.db.query(`
			UPDATE _logs
			SET log = JSON_ARRAY_APPEND(log, '$', CAST('${JSON.stringify(list[0])}' AS JSON))
			WHERE id = ${id} AND entity = '${entity}'
		`))
	}
	async fieldNames (entity) {
		return await this.db.query(`DESCRIBE ${entity}`)
		.then( TextRow => Array.from(TextRow).map( m => m.Field ))
	}
	async enums (entity) {
		return await this.db.query(`SHOW COLUMNS FROM ${entity} WHERE type LIKE 'enum(%'`)
		.then( TextRow => Array.from(TextRow))
		.then( list => list.reduce(
			(r, c) => ({...r, [c.Field]: c.Type.match(/'(.*?)'/g).map( m => m.replace(/'/g, "")) })
			, {}
		))
	}
	async cleanup (data, entity) {
		const fields = await fieldNames(entity)
		
		return fields.filter( k => data[k] ).reduce((r, k) => ({...r, [k]: data[k] }), {})
	}
}
