const db = require('../../common/db.js')

/* eslint-disable no-unused-vars */
exports.Categorias = class Categorias {
  constructor (options) {
    this.options = options || {};
  }

  async find (params) {
    return await db.query(`SELECT * FROM categorias`)
  }

  async get (id, params) {
    return {
      id, text: `A new message with ID: ${id}!`
    };
  }

  async create (data, params) {
    if (Array.isArray(data)) {
      return Promise.all(data.map(current => this.create(current, params)));
    }

    return data;
  }

  async update (id, data, params) {
    return data;
  }

  async patch (id, data, params) {
    return data;
  }

  async remove (id, params) {
    return { id };
  }
};
