// Initializes the `categorias` service on path `/categorias`
const { Categorias } = require('./categorias.class');
const hooks = require('./categorias.hooks');

module.exports = function (app) {
  const options = {
    paginate: app.get('paginate')
  };

  // Initialize our service with any options it requires
  app.use('/categorias', new Categorias(options, app));

  // Get our initialized service so that we can register hooks
  const service = app.service('categorias');

  service.hooks(hooks);
};
