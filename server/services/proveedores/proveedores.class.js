const db = require('../../common/db.js')
const fs = require('fs')
const path = require('path')


/* eslint-disable no-unused-vars */
class Service {
  constructor (options) {
    this.options = options || {}
  }

  async find ({ query }) {
    if (query.import) {
      const fileName = path.join(__dirname, 'Proveedor.listar_proveedor.json')
      const rawContents = fs.readFileSync(fileName)

      const fields = ['idProveedor', 'RazonSocial', 'NombreFantasia']
      const result = JSON.parse(rawContents)
      .map( m => fields.reduce((r, c) => ({...r, [c]: m[c] }), {}))
      
      result.map( async m => console.log(await db.query('INSERT IGNORE INTO proveedores SET ?', m )))

      return result
    }
    if (query.field && query.fuzzy) {
      console.log('proveedores/search', query)
      
      return await db.query(`SELECT * FROM proveedores WHERE ${query.field} LIKE '%${query.fuzzy}%'`)
    }
    return [{ error: 'Missing parameters: import=1 or field=RazonSocial&fuzzy=FRIGO'}]
  }

  async get (id, params) {
    return await db.query(`SELECT * FROM proveedores WHERE idProveedor = ${id}`)
    .then( list => list && list.length ? list[0] : {})
  }

  async create (data, params) {
    return await db.query(`INSERT IGNORE proveedores SET ?`, data)
    .then( result => ({...data, result }))
  }

  async update (id, data, params) {
    return data;
  }

  async patch (id, data, params) {
    return data;
  }

  async remove (id, params) {
    return { id };
  }
}

module.exports = function (options) {
  return new Service(options);
};

module.exports.Service = Service;
