// Initializes the `proveedores` service on path `/proveedores`
const createService = require('./proveedores.class.js');
const hooks = require('./proveedores.hooks');

module.exports = function (app) {
  
  const paginate = app.get('paginate');

  const options = {
    paginate
  };

  // Initialize our service with any options it requires
  app.use('/proveedores', createService(options));

  // Get our initialized service so that we can register hooks
  const service = app.service('proveedores');

  service.hooks(hooks);
};
