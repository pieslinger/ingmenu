const proveedores = require('./proveedores/proveedores.service.js');
const compras = require('./compras/compras.service.js');
const insumos = require('./insumos/insumos.service.js');
const ventas = require('./ventas/ventas.service.js');
const productos = require('./productos/productos.service.js');
const recetas = require('./recetas/recetas.service.js');
const admins = require('./admins/admins.service.js');
const colppy = require('./colppy/colppy.service.js');
const comprasInsumos = require('./compras-insumos/compras-insumos.service.js');
const ventasProductos = require('./ventas-productos/ventas-productos.service.js');
const dashboard = require('./dashboard/dashboard.service.js');
const consumos = require('./consumos/consumos.service.js');
const categorias = require('./categorias/categorias.service.js');
// eslint-disable-next-line no-unused-vars
module.exports = function (app) {
  app.configure(proveedores);
  app.configure(compras);
  app.configure(insumos);
  app.configure(ventas);
  app.configure(productos);
  app.configure(recetas);
  app.configure(admins);
  app.configure(colppy);
  app.configure(comprasInsumos);
  app.configure(ventasProductos);
  app.configure(dashboard);
  app.configure(consumos);
  app.configure(categorias);
};
