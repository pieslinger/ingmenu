const db = require('../../common/db.js')
const { cleanup } = require('../../common/db.js')

/* eslint-disable no-unused-vars */
exports.VentasProductos = class VentasProductos {
  constructor (options) {
    this.options = options || {};
  }

  async find ({ query }) {
		if (query.id_venta) return await db.query(`
      SELECT P.*, IFNULL(V.cantidad, 0) AS cantidad
      FROM productos_vue P 
      JOIN ventas_productos V
        ON P.id = V.id_producto AND V.id_venta = ${query.id_venta}
      ORDER BY P.categoria, P.tipo, P.orden, P.producto
    `)
    return []
  }

  async get (id, params) {
    return {
      id, text: `A new message with ID: ${id}!`
    };
  }

  async create (data, params) {
    if (Array.isArray(data)) {
      return Promise.all(data.map(current => this.create(current, params)));
    }

    return data;
  }

  async update (_, data, params) {
    if (Array.isArray(data)) {
      return Promise.all(data.map( current => this.update(_, current, params)))
    }
    const venta = await cleanup(data, 'ventas_productos')
    const result = await db.query(`REPLACE ventas_productos SET ?`, venta)

    return {...data, result }
  }

  async patch (id, data, params) {
    return data;
  }

  async remove (id, params) {
    return { id };
  }
}
