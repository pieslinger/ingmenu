// Initializes the `ventas-productos` service on path `/ventas-productos`
const { VentasProductos } = require('./ventas-productos.class');
const hooks = require('./ventas-productos.hooks');

module.exports = function (app) {
  
  const paginate = app.get('paginate');

  const options = {
    paginate
  };

  // Initialize our service with any options it requires
  app.use('/ventas-productos', new VentasProductos(options, app));

  // Get our initialized service so that we can register hooks
  const service = app.service('ventas-productos');

  service.hooks(hooks);
};
