const db = require('../../common/db.js')
const { cleanup } = require('../../common/db.js')

/* eslint-disable no-unused-vars */
exports.ComprasInsumos = class ComprasInsumos {
  constructor (options) {
    this.options = options || {};
  }

	async find ({ query }) {
    if (query.id_compra) return await db.query(`
			SELECT C.id_insumo, I.insumo, C.cantidad, C.unidades, C.costo
			FROM compras_insumos C
			JOIN insumos I ON C.id_insumo = I.id
			WHERE C.id_compra = '${query.id_compra}'
		`)
		return []
	}

  async get (id, params) {
    return {
      id, text: `A new message with ID: ${id}!`
    };
  }

  async create (data, params) {
    if (Array.isArray(data)) {
      return Promise.all(data.map( current => this.create(current, params)))
    }
    return data;
  }

  async update (_, data, params) {
    if (Array.isArray(data)) {
      return Promise.all(data.map( current => this.update(_, current, params)))
    }
    const compra = await cleanup(data, 'compras_insumos')
    const result = await db.query(`REPLACE compras_insumos SET ?`, compra)

    return {...data, result }
  }

  async patch (id, data, params) {
    return data;
  }

  async remove (id, params) {
    return { id };
  }
}
