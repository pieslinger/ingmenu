// Initializes the `compras_insumos` service on path `/compras-insumos`
const { ComprasInsumos } = require('./compras-insumos.class');
const hooks = require('./compras-insumos.hooks');

module.exports = function (app) {
  
  const paginate = app.get('paginate');

  const options = {
    paginate
  };

  // Initialize our service with any options it requires
  app.use('/compras-insumos', new ComprasInsumos(options, app));

  // Get our initialized service so that we can register hooks
  const service = app.service('compras-insumos');

  service.hooks(hooks);
};
