const insertInsumos = async (context) => {
  const { app, data } = context

  if (Array.isArray(data)) return context //  ignore list, work with items
  
  Object.assign(data, { id: data.id_insumo, id_insumo: undefined }) // insumos usa id

  const result = await app.service('insumos').create(data)

  Object.assign(data, { id_insumo: result.id, id: undefined }) // compras_insumos usa id_insumo

  return {...context, data }
}

const fixCantidadNumber = context => {
	const { result } = context
	const fixIt = item => item.cantidad ? {...item, cantidad: Number(item.cantidad) } : item

	return {...context, result: Array.isArray(result) ? result.map( m => fixIt(m)) : fixIt(result) }
}
const fixCostoNumber = context => {
	const { result } = context
	const fixIt = item => item.costo ? {...item, costo: Number(item.costo) } : item

	return {...context, result: Array.isArray(result) ? result.map( m => fixIt(m)) : fixIt(result) }
}

module.exports = {
  before: {
    all: [],
    find: [],
    get: [],
    create: [insertInsumos],
    update: [insertInsumos],
    patch: [],
    remove: []
  },

  after: {
    all: [],
    find: [fixCantidadNumber, fixCostoNumber],
    get: [],
    create: [],
    update: [],
    patch: [],
    remove: []
  },

  error: {
    all: [],
    find: [],
    get: [],
    create: [],
    update: [],
    patch: [],
    remove: []
  }
};
